//
//  SubscriptionSerachView.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 29/10/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import UIKit

protocol SubscriptionSerachViewProtocol: class {
}

final class SubscriptionSerachView: UIView {
    weak var delegate: SubscriptionSerachViewProtocol?

    @IBOutlet weak var textFieldSearch: UITextField! {
        didSet {
            textFieldSearch.placeholder = localized("subscriptions.search")
            
            if let placeholder = textFieldSearch.placeholder {
                let color = UIColor(rgb: 0x9AB1BF, alphaVal: 1)
                textFieldSearch.attributedPlaceholder = NSAttributedString(string:placeholder, attributes: [NSForegroundColorAttributeName: color])
            }
        }
    }
}
