//
//  ChatHeaderSearchView.swift
//  Rocket.Chat
//
//  Created by Rafael Kellermann Streit on 05/12/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import UIKit

final class ChatHeaderSearchView: UIView {

    @IBOutlet weak var searchBar: UISearchBar!
}
