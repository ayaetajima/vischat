//
//  ChatTitleView.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 10/10/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import UIKit

protocol ChatTitleViewDelegate {
    
    func didTapSearchButton()
    func didTapProfileButton()
}

final class ChatTitleView: UIView {

    var delegate: ChatTitleViewDelegate?

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var labelTitle: UILabel! {
        didSet {
            labelTitle.textColor = .RCDarkGray()
        }
    }

    @IBOutlet weak var imageArrowDown: UIImageView! {
        didSet {
            imageArrowDown.image = imageArrowDown.image?.imageWithTint(.RCGray())
        }
    }

    var subscription: Subscription! {
        didSet {
            labelTitle.text = subscription.name

            switch subscription.type {
            case .channel:
                icon.image = UIImage(named: "Hashtag")?.imageWithTint(.RCGray())
                break
            case .directMessage:
                var color = UIColor.RCGray()

                if let user = subscription.directMessageUser {
                    color = { _ -> UIColor in
                        switch user.status {
                        case .online: return .RCOnline()
                        case .offline: return .RCGray()
                        case .away: return .RCAway()
                        case .busy: return .RCBusy()
                        }
                    }()
                }

                icon.image = UIImage(named: "mention")?.imageWithTint(color)
                break
            case .group:
                icon.image = UIImage(named: "Lock")?.imageWithTint(.RCGray())
                break
            }
        }
    }
    
    @IBOutlet weak var searchButton: UIButton!    
    @IBOutlet weak var profileButton: UIButton!
    @IBAction func didTapSearch() {
         self.delegate?.didTapSearchButton()
    }
    
    @IBAction func didTapProfile() {
        self.delegate?.didTapProfileButton()
    }
}
