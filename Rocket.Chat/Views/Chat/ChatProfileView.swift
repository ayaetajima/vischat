//
//  ChatProfileView.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 10/10/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import UIKit

protocol ChatProfileViewDelegate {
    func didTapProfileListButton(tag: ProfileButtonTag)
    func didTapProfileSettingButton()
    func didTapProfileStatusButton()
}

enum ProfileButtonTag: Int {
    case memberList = 0
    case mention = 1
    case bookmark = 2
    case file = 3
    case pin = 4
}

final class ChatProfileView: UIView {

    var delegate: ChatProfileViewDelegate?

    @IBOutlet weak var clearView: UIView!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userIDLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var memberListButton: UIButton!
    @IBOutlet weak var mentionButton: UIButton!
    @IBOutlet weak var bookmarkButton: UIButton!
    @IBOutlet weak var fileButton: UIButton!
    @IBOutlet weak var pinButton: UIButton!
    @IBOutlet weak var settingButton: UIButton!
    
    @IBOutlet weak var avatarViewContainer: UIView! {
        didSet {
            if let avatarView = AvatarView.instantiateFromNib() {
                avatarView.frame = avatarViewContainer.bounds
                avatarViewContainer.layer.cornerRadius = avatarView.frame.size.width/2
                avatarViewContainer.layer.masksToBounds = true

                avatarViewContainer.addSubview(avatarView)
                self.avatarView = avatarView
            }
        }
    }
    
    weak var avatarView: AvatarView! 
    let menuList = ["メンバーリスト","メンション","ブックマーク","ファイル","ピンしたメッセージ"]
    let imageList = ["member_list","mention","star","file","pin"]

    @IBAction func didTapSettingButton() {
        self.delegate?.didTapProfileSettingButton()
    }

    @IBAction func didTapUserStatusButton() {
        self.delegate?.didTapProfileStatusButton()
    }
    
    @IBAction func didTapListButton(sender: UIButton) {
        self.delegate?.didTapProfileListButton(tag: ProfileButtonTag(rawValue: sender.tag)!)
    }
}

extension ChatProfileView : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return menuList.count
        }else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }else {
            return 20
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let text :String!
        let image: String!

        if indexPath.section == 0 {
            text = "ステータス"
            image = "status"
        }
        else if indexPath.section == 1 {
            text = menuList[indexPath.row]
            image = imageList[indexPath.row]
        }else {
            text = "設定"
            image = "setting"
        }
        
        if let cell = tableView.dequeueReusableCell(withIdentifier:"cell") {
            cell.textLabel?.text = text
            cell.imageView?.image = UIImage(named:image)
            
            return cell
        }else {
            let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
            cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            cell.textLabel?.textColor = UIColor.darkGray
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
            cell.textLabel?.text = text
            cell.imageView?.image = UIImage(named:image)
            
            return cell
        }
        
    }
}

extension ChatProfileView : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.section {
        case 0:
            self.delegate?.didTapProfileStatusButton()
           break
        case 1:
            self.delegate?.didTapProfileListButton(tag: ProfileButtonTag(rawValue: indexPath.row)!)
            break
        case 2:
            self.delegate?.didTapProfileSettingButton()
            break
        default:
            break
        }
    }
}

