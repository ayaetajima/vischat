//
//  ChatEmojiCollectionView.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 10/10/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import UIKit
import RealmSwift
import SDWebImage

enum EmojiType :Int{
    case emoji, customStamp
}

protocol ChatEmojiCollectionViewDelegate {
    func didSelectEmojiCell(emojiString:String)
    func didSelectCustomStampCell(stamp:CustomStamp)
}

final class ChatEmojiCollectionView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet var collectionView: UICollectionView!{
        didSet{
            collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        }
    }
    var delegate:ChatEmojiCollectionViewDelegate!
    var serverURL:String!
    var cellSize: CGFloat!
    var categoryName:String!{
        didSet{
            if categoryName == "rocket" {
                // スタンプのカテゴリはロケット
                self.emojiType = .customStamp
                self.cellSize = (collectionView.frame.width-10.0)/4.0
                self.collectionView.reloadData()
                
                /* スタンプは毎回取得するように変更
                 if let stamps = try?Realm().objects(CustomStamp.self) {
                 if stamps.count > 0 {
                 self.stampList = Array(stamps)
                 self.collectionView.reloadData()
                 return
                 }
                 }
                 MessageManager.getListEmojiCustom({(list) in
                 self.stampList = list
                 self.collectionView.reloadData()
                 })*/
            }else {
                self.emojiType = .emoji
                self.cellSize = (collectionView.frame.width-10.0)/8.0
                self.collectionView.reloadData()
            }
        }
    }
    
    var emojiType:EmojiType!
    var stampList:[CustomStamp]?
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if emojiType == .customStamp {
            return (stampList?.count)!
        }
        
        return (EmojiCategories.emojisByCategory[categoryName]?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: cellSize, height: cellSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        for view in cell.subviews{
            view.removeFromSuperview()
        }
        
        if self.emojiType == .emoji {
            // 絵文字
            let emojiStr = ":" + (EmojiCategories.emojisByCategory[categoryName]?[indexPath.row])! + ":"
            let label = UILabel(frame: CGRect(x:0 ,y:0, width:cell.frame.width, height:cell.frame.width))
            label.text = Emojione.transform(string: emojiStr)
            label.font = UIFont.systemFont(ofSize: 30)
            cell.addSubview(label)
        } else {
            // カスタム絵文字（スタンプ）
            let imageView = UIImageView()
            imageView.frame = CGRect(x:0 ,y:0, width:cell.frame.width, height:cell.frame.width)
            let imageURL = self.stampList?[indexPath.row].fullImageURL()
            
            // キャッシュのクリア（差し替え対応）
            let imageCache = SDImageCache.shared()
            imageCache?.removeImage(forKey: imageURL?.absoluteString)
            
            imageView.sd_setImage(with: imageURL, completed: {_ in
            })
            cell.addSubview(imageView)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if emojiType == .emoji {
            // 選択した絵文字を反映させる
            let emojiStr = ":" + (EmojiCategories.emojisByCategory[categoryName]?[indexPath.row])! + ":"
            self.delegate.didSelectEmojiCell(emojiString: emojiStr)
        }else {
            // スタンプを送信
            self.delegate.didSelectCustomStampCell(stamp: (self.stampList?[indexPath.row])!)
        }
    }
}

