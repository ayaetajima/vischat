//
//  ChatEmojiView.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 10/10/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import UIKit

protocol ChatEmojiViewDelegate {
    func didSelectEmojiButton(emojiString:String)
    func didSelectCustomStampButton(stamp:CustomStamp)
}

final class ChatEmojiView: UIView, UIScrollViewDelegate{

    var delegate: ChatEmojiViewDelegate?
    var categoryPages = [ChatEmojiCollectionView]()
    var categoryLabels = [UILabel]()
    var serverURL = ""
    var stampList: [CustomStamp]! {
        didSet {
            if categoryPages.count > 0{
                categoryPages[0].stampList = stampList
                categoryPages[0].collectionView.reloadData()
            }
        }
    }
    
    @IBOutlet var navigationBar: UINavigationItem!
    
    @IBOutlet var gridScrollView: UIScrollView!
    @IBOutlet var categoryScrollView: UIScrollView!
    
    @IBAction func didTapCloseButton() {
        self.isHidden = true
    }
    
    func setEmojiCategories() {
        // 下のスクロールバーにカテゴリのアイコンを並べて表示
        let buttonSize = categoryScrollView.frame.height

        categoryScrollView.contentSize.width = buttonSize * CGFloat(EmojiCategories.emojiCategories.count)
        gridScrollView.contentSize.width = gridScrollView.frame.width * CGFloat(EmojiCategories.emojiCategories.count)
        
        for i in 0..<EmojiCategories.emojiCategories.count {
            let array = EmojiCategories.emojiCategories[i]
            let key = array[0]
            
            // カテゴリボタン生成
            let button = UIButton(type: .custom)
            button.frame = CGRect(x:CGFloat(i) * buttonSize, y:0, width:buttonSize, height:buttonSize)
            button.addTarget(self, action:#selector(didTapCategoryButton), for:.touchUpInside)
            button.tag = i
            
            let textLabel = UILabel(frame: CGRect(x:0,y:0, width:button.frame.width, height:button.frame.height))
            textLabel.font = Fontello.fontOfSize(20, name: "fontello")
            textLabel.textColor = UIColor.gray
            textLabel.text = array[1]
            textLabel.textAlignment = .center
            button.addSubview(textLabel)
            
            categoryLabels.append(textLabel)
            categoryScrollView.addSubview(button)
            focusCategoryButton(selectedNum: 0)
            
            // カテゴリページ生成
            let page = ChatEmojiCollectionView.instantiateFromNib()
            page?.frame = CGRect(x: CGFloat(i) * gridScrollView.frame.width, y: 0, width: gridScrollView.frame.width, height: gridScrollView.frame.height)
            page?.categoryName = key
            page?.tag = i
            page?.delegate = self
            page?.serverURL = serverURL
            
            if i == 0 {
                page?.stampList = stampList
            }
            categoryPages.append(page!)
            gridScrollView.addSubview(page!)            
        }
    }
    
    func didTapCategoryButton(sender: UIButton) {
        focusCategoryButton(selectedNum: sender.tag)
        gridScrollView.setContentOffset(categoryPages[sender.tag].frame.origin, animated: true)
    }
    
    func focusCategoryButton(selectedNum: Int) {
        for i in 0 ..< categoryLabels.count {
            let label = categoryLabels[i]
            if i == selectedNum {
                label.textColor = UIColor.black
            }else {
                label.textColor = UIColor.gray
            }
        }
        
        if selectedNum == 0 {
            self.navigationBar.title = "スタンプ"
        } else {
            self.navigationBar.title = "絵文字"
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        // カテゴリアイコンのフォーカスを変える
        let pageNum = Int(scrollView.contentOffset.x / self.frame.width)
        focusCategoryButton(selectedNum: pageNum)
    }
}

extension ChatEmojiView: ChatEmojiCollectionViewDelegate {
    func didSelectEmojiCell(emojiString: String) {
        self.delegate?.didSelectEmojiButton(emojiString: emojiString)
    }
    
    func didSelectCustomStampCell(stamp:CustomStamp) {
        self.delegate?.didSelectCustomStampButton(stamp: stamp)
    }
}
