//
//  SubscriptionUserStatusView.swift
//  Rocket.Chat
//
//  Created by Rafael Kellermann Streit on 13/02/17.
//  Copyright © 2017 Rocket.Chat. All rights reserved.
//

import UIKit

protocol SubscriptionUserStatusViewProtocol: class {
    func userDidPressedOption()
    func userDidPressedClose()
}

final class SubscriptionUserStatusView: UIView {

    weak var delegate: SubscriptionUserStatusViewProtocol?
    weak var parentController: UIViewController?

    @IBOutlet weak var currentStatus: UIView!
    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var buttonOnline: UIButton!
    @IBOutlet weak var labelOnline: UILabel! {
        didSet {
            labelOnline.text = localized("user_menu.online")
        }
    }

    @IBOutlet weak var buttonAway: UIView!
    @IBOutlet weak var labelAway: UILabel! {
        didSet {
            labelAway.text = localized("user_menu.away")
        }
    }

    @IBOutlet weak var buttonBusy: UIView!
    @IBOutlet weak var labelBusy: UILabel! {
        didSet {
            labelBusy.text = localized("user_menu.busy")
        }
    }

    @IBOutlet weak var buttonInvisible: UIView!
    @IBOutlet weak var labelInvisible: UILabel! {
        didSet {
            labelInvisible.text = localized("user_menu.invisible")
        }
    }
    
    // MARK: IBAction
    @IBAction func buttonCloseDidPressed(_ sender: Any) {
        self.delegate?.userDidPressedClose()
    }

    @IBAction func buttonOnlineDidPressed(_ sender: Any) {
        UserManager.setUserStatus(status: .online) { [weak self] (_) in
            self?.delegate?.userDidPressedOption()
        }
    }

    @IBAction func buttonAwayDidPressed(_ sender: Any) {
        UserManager.setUserStatus(status: .away) { [weak self] (_) in
            self?.delegate?.userDidPressedOption()
        }
    }

    @IBAction func buttonBusyDidPressed(_ sender: Any) {
        UserManager.setUserStatus(status: .busy) { [weak self] (_) in
            self?.delegate?.userDidPressedOption()
        }
    }

    @IBAction func buttonInvisibleDidPressed(_ sender: Any) {
        UserManager.setUserStatus(status: .offline) { [weak self] (_) in
            self?.delegate?.userDidPressedOption()
        }
    }

}
