//
//  SubscriptionCreateChannelView.swift
//  Rocket.Chat
//
//  Created by Rafael Kellermann Streit on 13/02/17.
//  Copyright © 2017 Rocket.Chat. All rights reserved.
//

import UIKit

protocol SubscriptionCreateChannelViewProtocol: class {
    func createChannelButtonPressed(name:String, isPrivate:Bool, isReadOnly:Bool)
    func closeCreateChannelView()
}

final class SubscriptionCreateChannelView: UIView {

    weak var delegate: SubscriptionCreateChannelViewProtocol?
    weak var parentController: UIViewController?

    @IBOutlet weak var labelTitle: UILabel! {
        didSet {
            labelTitle.text = localized("create_channel.title")
        }
    }

    @IBOutlet weak var textFieldChannelName: UITextField! {
        didSet {
            textFieldChannelName.placeholder = localized("create_channel.channel")
        }
    }

    @IBOutlet weak var nameLengthLabel: UILabel!
    @IBOutlet weak var labelPrivate: UILabel! {
        didSet {
            labelPrivate.text = localized("create_channel.private")
        }
    }
    @IBOutlet weak var switchPrivate: UISwitch!
    
    @IBOutlet weak var labelReadOnly: UILabel! {
        didSet {
            labelReadOnly.text = localized("create_channel.read_only")
        }
    }
    @IBOutlet weak var switchReadOnly: UISwitch!

    @IBOutlet weak var buttonCreate: UIButton!
    @IBOutlet weak var buttonCancel: UIButton!


    // MARK: IBAction

    @IBAction func buttonCreateDidPressed(_ sender: Any) {
        self.delegate?.createChannelButtonPressed(name: self.textFieldChannelName.text!, isPrivate: self.switchPrivate.isOn, isReadOnly: self.switchReadOnly.isOn)
    }

    @IBAction func buttonCancelDidPressed(_ sender: Any) {
        self.delegate?.closeCreateChannelView()
    }

}

extension SubscriptionCreateChannelView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        if string == "\n" {
            return false
        }

        if currentText.characters.count >= 100 && string.characters.count > 0{
            return false
        }

        var count = currentText.characters.count + string.characters.count
        if string == "" {
            count = currentText.characters.count - 1
        }
        nameLengthLabel.text = "\(count)/100"
        return true
    }
}
