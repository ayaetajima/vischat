//
//  SubscriptionDMSuggestView.swift
//  Rocket.Chat
//
//  Created by Rafael Kellermann Streit on 13/02/17.
//  Copyright © 2017 Rocket.Chat. All rights reserved.
//

import UIKit
import RealmSwift

protocol SubscriptionDMSuggestViewProtocol: class {
    func didSelect(subscription:Subscription)
    func closeSuggestView()
}

enum SuggesuSerachType: Int {
    case directMessage, inviteRoom
}

// DM名前検索用view
final class SubscriptionDMSuggestView: UIView {

    weak var delegate: SubscriptionDMSuggestViewProtocol?

    @IBOutlet weak var userListView: UITableView!
    @IBOutlet weak var searchNameView: UITextField!
    
    var isSearchingLocally = false
    var isSearchingRemotely = false
    var searchResult : [Subscription]?
    var subscriptions: Results<Subscription>?
    var selectedSubscription: Subscription!
    var searchType: SuggesuSerachType!
    var roomMember: List<User>?
    
    @IBAction func didTapSelectButton() {
        if selectedSubscription != nil {
            self.delegate?.didSelect(subscription: selectedSubscription)
        }
    }
    
    @IBAction func didTapCloseButton() {
        self.delegate?.closeSuggestView()
    }
    
    func searchBy(_ text: String = "") {
        guard let auth = AuthManager.isAuthenticated() else { return }
        subscriptions = auth.subscriptions.filter("name CONTAINS %@", text)

        if text.characters.count == 0 {
            isSearchingLocally = false
            isSearchingRemotely = false
            searchResult = []
            
            userListView.reloadData()
            return
        }
        
        if subscriptions?.count == 0 {
            searchOnSpotlight(text)
            return
        }
        
        isSearchingLocally = true
        isSearchingRemotely = false
        
        appendUserSubscriptions()
        userListView.reloadData()
    }
    
    func searchOnSpotlight(_ text: String = "") {
        SubscriptionManager.spotlight(text, rooms: false, users: true) { [weak self] result in
            let currentText = self?.searchNameView.text ?? ""
            
            if currentText.characters.count == 0 {
                return
            }
            
            self?.isSearchingRemotely = true
            self?.searchResult = result
            self?.userListView.reloadData()
        }
    }
    
    func appendUserSubscriptions() {
        guard let subscriptions = subscriptions else { return }
        let orderSubscriptions = isSearchingRemotely ? searchResult : Array(subscriptions.sorted(byKeyPath: "name", ascending: true))
        
        for subscription in orderSubscriptions ?? [] {
            if !isSearchingLocally && !subscription.open {
                continue
            }
            
            // DMのみ格納
            if subscription.type == .directMessage {
                if searchResult == nil {
                    searchResult = []
                }
                
                // メンバー招待の場合、すでにメンバーになっている人はサジェストから除外
                var needAppend = true
                if searchType == .inviteRoom {
                    for user in roomMember! {
                        if subscription.name == user.username {
                            needAppend = false
                            break
                        }
                    }
                }
                
                if needAppend {
                    searchResult?.append(subscription)
                }
            }
        }
    }
}

extension SubscriptionDMSuggestView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
        
        if string == "\n" {
            if currentText.characters.count > 0 {
//                searchOnSpotlight(currentText)
            }
            
            return false
        }
        
        searchResult?.removeAll()
        searchBy(prospectiveText)
        return true
    }
}

extension SubscriptionDMSuggestView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let subscription = searchResult?[indexPath.row]
        let user = User()
        user.username = subscription?.name
        user.name = subscription?.name

        if let cell = tableView.dequeueReusableCell(withIdentifier: ChatMemberListCell.identifier) as? ChatMemberListCell {
            cell.user = user
            return cell
        }else {
            let cell = ChatMemberListCell.instantiateFromNib()
            cell?.user = user
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchResult != nil {
            let num = searchResult!.count
            userListView.isHidden = !(num > 0)
            
            if num < 6 {
                userListView.frame.size.height = ChatMemberListCell.minimumHeight*CGFloat(num)
            }else {
                userListView.frame.size.height = ChatMemberListCell.minimumHeight*CGFloat(5)
            }
            return num
        }else {
            userListView.isHidden = true
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ChatMemberListCell.minimumHeight
    }
}

extension SubscriptionDMSuggestView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.isHidden = true
        
        self.selectedSubscription = searchResult?[indexPath.row]
        
        let cell = tableView.cellForRow(at: indexPath) as! ChatMemberListCell
        searchNameView.text = cell.user.username
    }
}
