//
//  ChannelInfoDescriptionCell.swift
//  Rocket.Chat
//
//  Created by Rafael Kellermann Streit on 10/03/17.
//  Copyright © 2017 Rocket.Chat. All rights reserved.
//

import UIKit

struct ChannelInfoDescriptionCellData: ChannelInfoCellDataProtocol {
    let cellType = ChannelInfoDescriptionCell.self
    var title: String?
    var description: String?
}

class ChannelInfoDescriptionCell: UITableViewCell, ChannelInfoCellProtocol {

    static let identifier = "kChannelInfoCellDescription"
    static let defaultHeight: CGFloat = 100

    @IBOutlet weak var textDescription: UITextView!
    
    var data: ChannelInfoDescriptionCellData? {
        didSet {
//            labelTitle.text = data?.title
            textDescription.text = data?.description
        }
    }

    func getHeight() -> CGFloat{
        if textDescription.frame.size.height < ChannelInfoDescriptionCell.defaultHeight {
            return ChannelInfoDescriptionCell.defaultHeight
        }else {
            return textDescription.frame.size.height + 10
        }
    }
}
