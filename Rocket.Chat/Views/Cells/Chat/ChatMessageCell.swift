//
//  ChatTextCell.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 7/25/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import UIKit

protocol ChatMessageCellProtocol: ChatMessageURLViewProtocol, ChatMessageVideoViewProtocol, ChatMessageImageViewProtocol, ChatMessageTextViewProtocol {
    func openURL(url: URL)
    func didTapIconImage(_ message: Message, view: UIView, recognizer: UITapGestureRecognizer)
    func didTapTextMessage(_ message: Message, view: UIView, recognizer: UITapGestureRecognizer)
    func didLongTapTextMessage(_ message: Message, view: UIView, recognizer: UILongPressGestureRecognizer)
}

final class ChatMessageCell: UICollectionViewCell {

    static let minimumHeight = CGFloat(55)
    static let identifier = "ChatMessageCell"

    weak var delegate: ChatMessageCellProtocol?
    var message: Message! {
        didSet {
            updateMessageInformation()
        }
    }
    
    weak var tapTextGesture: UITapGestureRecognizer?
    weak var longtapTextGesture: UILongPressGestureRecognizer?

    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var myBaseView: UIView!
    
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var avatarViewContainer: UIView! {
        didSet {
            if let avatarView = AvatarView.instantiateFromNib() {
                avatarView.frame = avatarViewContainer.bounds
                avatarViewContainer.layer.cornerRadius = avatarView.frame.size.width/2
                avatarViewContainer.layer.masksToBounds = true

                avatarViewContainer.addSubview(avatarView)
                self.avatarView = avatarView
            }
        }
    }

    weak var tapIconGesture: UITapGestureRecognizer?
    weak var avatarView: AvatarView! {
        didSet {            
            if self.tapIconGesture == nil {
                let gesture = UITapGestureRecognizer(target: self, action: #selector(iconDidTapped(recognizer:)))
                gesture.delegate = self
                avatarView.addGestureRecognizer(gesture)
                self.tapIconGesture = gesture
            }
        }
    }

    @IBOutlet weak var imageBackGround: UIImageView! {
        didSet {
            let image = UIImage.init(named: "chat_other_hukidashi")
            imageBackGround.image = image?.stretchableImage(withLeftCapWidth: 5, topCapHeight: 5)
        }
    }
    @IBOutlet weak var myImageBackGround: UIImageView! {
        didSet {
            let image = UIImage.init(named: "chat_my_hukidashi")
            myImageBackGround.image = image?.stretchableImage(withLeftCapWidth: 5, topCapHeight: 5)
        }
    }
    
    @IBOutlet weak var stampImageView: UIImageView!
    @IBOutlet weak var myStampImageView: UIImageView!
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var myLabelDate: UILabel!

    @IBOutlet weak var labelRead: UILabel!
    @IBOutlet weak var myLabelRead: UILabel!

    @IBOutlet weak var labelText: UITextView! {
        didSet {
            labelText.textContainerInset = .zero
            labelText.delegate = self
        }
    }
    @IBOutlet weak var myLabelText: UITextView! {
        didSet {
            myLabelText.textContainerInset = .zero
            myLabelText.delegate = self
        }
    }

    @IBOutlet weak var mediaViews: UIStackView!
    @IBOutlet weak var myMediaViews: UIStackView!
    @IBOutlet weak var labelTextHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var myLabelTextHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mediaViewsHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var myMediaViewsHeightConstraint: NSLayoutConstraint!
    
    static var textHeight: CGFloat!

    static func cellMediaHeightFor(message: Message, grouped: Bool = true) -> CGFloat {
        
        if message.internalType == "stamp" {
            return CGFloat(150)
        }
        
        let fullWidth = UIScreen.main.bounds.size.width
        var font :UIFont!
        if message.isSystemMessage() {
            font = MessageTextFontAttributes.italicFont
        } else {
            font = MessageTextFontAttributes.defaultFont
        }
        var total = UILabel.heightForView(
            message.textNormalized(),
            font: font,
            width: fullWidth - 60
        )

        self.textHeight = total

        for url in message.urls {
            guard url.isValid() else { continue }
            total += ChatMessageURLView.defaultHeight
        }
        
        
        for attachment in message.attachments {
            let type = attachment.type

            if type == .textAttachment {
                total += ChatMessageTextView.heightFor(attachment)
            }

            if type == .image {
                total += ChatMessageImageView.defaultHeight
            }

            if type == .video {
                total += ChatMessageVideoView.defaultHeight
            }
        }
        
        let isMyMessage = (AuthManager.isAuthenticated()?.userId == message.user?.identifier)
        if !isMyMessage {
            total += 20 // usernameの分
        }
        return total + 30
    }

//    func setBackGroundImageHeight(message: Message) {
//        imageBackGround.frame.size.height = ChatMessageCell.cellMediaHeightFor(message: message)
//    }
    
    override func prepareForReuse() {
        labelUsername.text = ""
        labelText.text = ""
        labelDate.text = ""
        labelRead.text = ""
        myLabelText.text = ""
        myLabelRead.text = ""
        myLabelDate.text = ""
        
        for view in mediaViews.arrangedSubviews {
            view.removeFromSuperview()
        }
        for view in myMediaViews.arrangedSubviews {
            view.removeFromSuperview()
        }
    }

    func insertAttachments(baseStackView: UIStackView, constraint: NSLayoutConstraint) {
        var mediaViewHeight = CGFloat(0)

        message.urls.forEach { url in
            guard url.isValid() else { return }
            if let view = ChatMessageURLView.instantiateFromNib() {
                view.url = url
                view.delegate = delegate

                baseStackView.addArrangedSubview(view)

                mediaViewHeight += ChatMessageURLView.defaultHeight
            }
        }

        message.attachments.forEach { attachment in
            let type = attachment.type

            switch type {
            case .textAttachment:
                if let view = ChatMessageTextView.instantiateFromNib() {
                    view.attachment = attachment
                    view.delegate = delegate
                    view.translatesAutoresizingMaskIntoConstraints = false

                    baseStackView.addArrangedSubview(view)

                    mediaViewHeight += ChatMessageTextView.heightFor(attachment)
                }
                break

            case .image:
                if let view = ChatMessageImageView.instantiateFromNib() {
                    view.attachment = attachment
                    view.delegate = delegate
                    view.translatesAutoresizingMaskIntoConstraints = false

                    baseStackView.addArrangedSubview(view)

                    mediaViewHeight += ChatMessageImageView.defaultHeight
                }
                break

            case .video:
                if let view = ChatMessageVideoView.instantiateFromNib() {
                    view.attachment = attachment
                    view.delegate = delegate
                    view.translatesAutoresizingMaskIntoConstraints = false

                    baseStackView.addArrangedSubview(view)

                    mediaViewHeight += ChatMessageVideoView.defaultHeight
                }
                break

            default:
                return
            }
        }

        constraint.constant = CGFloat(mediaViewHeight)
    }

    func insertStamp(imageView: UIImageView) {
        let path = NSString(string: message.text)
        let ext = path.pathExtension
        let name = path.replacingOccurrences(of: "." + ext, with: "")

        let stamp = CustomStamp()
        stamp.name = name
        stamp.ext = ext
        let imageURL = stamp.fullImageURL()
        
        imageView.sd_setImage(with: imageURL, completed: {_ in
            imageView.frame.size.height = 150
            imageView.contentMode = .scaleAspectFit
        })
    }
    
    fileprivate func updateMessageInformation() {
        guard delegate != nil else { return }

        if AuthManager.isAuthenticated() == nil {
            return
        }
        
        if self.tapTextGesture == nil {
            let gesture = UITapGestureRecognizer(target: self, action: #selector(textDidTapped(recognizer:)))
            gesture.delegate = self
            self.contentView.addGestureRecognizer(gesture)
            self.tapTextGesture = gesture
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"

        if let createdAt = message.createdAt {
            labelDate.text = formatter.string(from: createdAt)
            myLabelDate.text = labelDate.text
        }

        avatarView.imageURL = URL(string: message.avatar)
        avatarView.user = message.user

        if message.alias.characters.count > 0 {
            labelUsername.text = message.alias
        } else {
            labelUsername.text = message.user?.username
        }

        let num = message.getReaderComponents().count
        if num > 0 {
            labelRead.text = "既読" + "\(num)"
        } else {
            labelRead.text = ""
        }
        myLabelRead.text = labelRead.text
        
        // 自分のメッセージと他人のメッセージでレイアウト変更
        var textColor :UIColor!
        var baseStackView :UIStackView!
        var constraint :NSLayoutConstraint!

        let isMyMessage = (AuthManager.isAuthenticated()?.userId == message?.user?.identifier)
        myBaseView.isHidden = !isMyMessage
        baseView.isHidden = isMyMessage

        if isMyMessage {
            // 自分のメッセージの場合
            baseStackView = myMediaViews
            textColor = MessageTextFontAttributes.defaultMyFontColor
            constraint = myMediaViewsHeightConstraint
        } else {
            // 他人のメッセージの場合
            baseStackView = mediaViews
            textColor = MessageTextFontAttributes.defaultFontColor
            constraint = mediaViewsHeightConstraint
        }
        
        // ロングタップ検知
        // システムメッセージ・画像・音楽・動画・スタンプは検知しない
        // かつ自分のメッセージまたはadmin権限の時に検知可能
        let user = AuthManager.currentUser()!
        if (!message.isSystemMessage()
            && !(message.type == .image || message.type == .audio || message.type == .video || message.internalType == "stamp")
            && (isMyMessage || user.roles == "admin"))
        {
            if longtapTextGesture == nil {
                let gesture = UILongPressGestureRecognizer(target: self, action: #selector(myTextDidLongTapped(recognizer:)))
                gesture.delegate = self
                self.contentView.addGestureRecognizer(gesture)
                longtapTextGesture = gesture
            }
        } else {
            if longtapTextGesture != nil {
                self.contentView.removeGestureRecognizer(longtapTextGesture!)
            }
        }

        
        // スタンプの場合
        let isStamp = message.internalType == "stamp"
        myStampImageView.isHidden = !isStamp
        stampImageView.isHidden = !isStamp
        imageBackGround.isHidden = isStamp
        myImageBackGround.isHidden = isStamp
        
        if isStamp {
            if isMyMessage {
                insertStamp(imageView: myStampImageView)
            } else {
                insertStamp(imageView: stampImageView)
            }
            return
        }
        
        let text = NSMutableAttributedString(string: message.textNormalized())
        if self.message.isSystemMessage() {
            text.setFont(MessageTextFontAttributes.italicFont)
            text.setFontColor(MessageTextFontAttributes.systemFontColor)
        } else {
            text.setFont(MessageTextFontAttributes.defaultFont)
            text.setFontColor(textColor)
        }
        
        // テキストラベルの高さ設定
        labelText.attributedText = text.transformMarkdown()
        myLabelText.attributedText = text.transformMarkdown()
        
        if text.length == 0 {
            labelTextHeightConstraint.constant = 0
            myLabelTextHeightConstraint.constant = 0
        }else {
            labelTextHeightConstraint.constant = ChatMessageCell.textHeight
            myLabelTextHeightConstraint.constant = ChatMessageCell.textHeight
        }
        
        insertAttachments(baseStackView: baseStackView, constraint: constraint)
        self.layoutSubviews()
    }

    // MARK: Gesture method

    func iconDidTapped(recognizer: UITapGestureRecognizer) {
        // プロフィール画面に遷移
        delegate?.didTapIconImage(message, view: contentView, recognizer: recognizer)
    }
    
    func textDidTapped(recognizer: UITapGestureRecognizer) {
        // メッセージ詳細画面に遷移
        delegate?.didTapTextMessage(message, view: contentView, recognizer: recognizer)
    }
    
    func myTextDidLongTapped(recognizer: UILongPressGestureRecognizer) {
        //　編集ボタンを表示
        delegate?.didLongTapTextMessage(message, view: contentView, recognizer: recognizer)
    }
}

extension ChatMessageCell: UIGestureRecognizerDelegate {

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }

}

extension ChatMessageCell: UITextViewDelegate {

    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        if URL.scheme == "http" || URL.scheme == "https" {
            delegate?.openURL(url: URL)
            return false
        }

        return true
    }
}
