//
//  ChatPinnedMessageCell.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 7/25/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import UIKit

protocol ChatMessageListCellProtocol: ChatMessageURLViewProtocol, ChatMessageVideoViewProtocol, ChatMessageImageViewProtocol, ChatMessageTextViewProtocol {
    func openURL(url: URL)
    func handleLongPressMessageCell(_ message: Message, view: UIView, recognizer: UIGestureRecognizer)
}

final class ChatMessageListCell: UITableViewCell {

    static let minimumHeight = CGFloat(55)
    static let identifier = "ChatMessageListCell"

    weak var longPressGesture: UILongPressGestureRecognizer?
    var delegate: ChatMessageListCellProtocol?
    var message: Message! {
        didSet {
            updateMessageInformation()
        }
    }

    @IBOutlet weak var avatarViewContainer: UIView! {
        didSet {
            if let avatarView = AvatarView.instantiateFromNib() {
                avatarView.frame = avatarViewContainer.bounds
                avatarViewContainer.layer.cornerRadius = avatarView.frame.size.width/2
                avatarViewContainer.layer.masksToBounds = true

                avatarViewContainer.addSubview(avatarView)
                self.avatarView = avatarView
            }
        }
    }

    weak var avatarView: AvatarView!
    @IBOutlet var imageBackGround: UIImageView! {
        didSet{
            let image = UIImage.init(named: "chat_other_hukidashi")
            imageBackGround.image = image?.stretchableImage(withLeftCapWidth: 40, topCapHeight: 40)
        }
    }
    @IBOutlet weak var stampImageView: UIImageView!

    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var labelText: UITextView! {
        didSet {
            labelText.textContainerInset = .zero
            labelText.delegate = self
        }
    }

    @IBOutlet weak var mediaViews: UIStackView!
    @IBOutlet weak var mediaViewsHeightConstraint: NSLayoutConstraint!

    static func cellMediaHeightFor(message: Message, grouped: Bool = true) -> CGFloat {
        if message.internalType == "stamp" {
            return CGFloat(150)
        }

        let fullWidth = UIScreen.main.bounds.size.width
        var total = UILabel.heightForView(
            message.textNormalized(),
            font: UIFont.systemFont(ofSize: 14),
            width: fullWidth - 60
        ) + 35 + 10

        for url in message.urls {
            guard url.isValid() else { continue }
            total += ChatMessageURLView.defaultHeight
        }

        for attachment in message.attachments {
            let type = attachment.type

            if type == .textAttachment {
                total += ChatMessageTextView.heightFor(attachment)
            }

            if type == .image {
                total += ChatMessageImageView.defaultHeight
            }

            if type == .video {
                total += ChatMessageVideoView.defaultHeight
            }
        }
        
        return total
    }

//    func setBackGroundImageHeight(message: Message) {
//        imageBackGround.frame.size.height = ChatMessageListCell.cellMediaHeightFor(message: message)
//    }
    
    override func prepareForReuse() {
        labelUsername.text = ""
        labelText.text = ""
        labelDate.text = ""

        for view in mediaViews.arrangedSubviews {
            view.removeFromSuperview()
        }
    }

    func insertGesturesIfNeeded() {
        if self.longPressGesture == nil {
            let gesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPressMessageCell(recognizer:)))
            gesture.minimumPressDuration = 1
            gesture.delegate = self
            contentView.addGestureRecognizer(gesture)
            self.longPressGesture = gesture
        }
    }

    func insertAttachments() {
        var mediaViewHeight = CGFloat(0)

        message.urls.forEach { url in
            guard url.isValid() else { return }
            if let view = ChatMessageURLView.instantiateFromNib() {
                view.url = url
                view.delegate = delegate

                mediaViews.addArrangedSubview(view)
                mediaViewHeight += ChatMessageURLView.defaultHeight
            }
        }

        message.attachments.forEach { attachment in
            let type = attachment.type

            switch type {
            case .textAttachment:
                if let view = ChatMessageTextView.instantiateFromNib() {
                    view.attachment = attachment
                    view.delegate = delegate
                    view.translatesAutoresizingMaskIntoConstraints = false

                    mediaViews.addArrangedSubview(view)
                    mediaViewHeight += ChatMessageTextView.heightFor(attachment)
                }
                break

            case .image:
                if let view = ChatMessageImageView.instantiateFromNib() {
                    view.attachment = attachment
                    view.delegate = delegate
                    view.translatesAutoresizingMaskIntoConstraints = false

                    mediaViews.addArrangedSubview(view)
                    mediaViewHeight += ChatMessageImageView.defaultHeight
                }
                break

            case .video:
                if let view = ChatMessageVideoView.instantiateFromNib() {
                    view.attachment = attachment
                    view.delegate = delegate
                    view.translatesAutoresizingMaskIntoConstraints = false

                    mediaViews.addArrangedSubview(view)
                    mediaViewHeight += ChatMessageVideoView.defaultHeight
                }
                break

            default:
                return
            }
        }

        mediaViewsHeightConstraint.constant = CGFloat(mediaViewHeight)
    }

    func insertStamp(imageView: UIImageView) {
        let path = NSString(string: message.text)
        let ext = path.pathExtension
        let name = path.replacingOccurrences(of: "." + ext, with: "")
        
        let stamp = CustomStamp()
        stamp.name = name
        stamp.ext = ext
        let imageURL = stamp.fullImageURL()
        
        imageView.sd_setImage(with: imageURL, completed: {_ in
            imageView.frame.size.height = 150
            imageView.contentMode = .scaleAspectFit
        })
    }
    
    fileprivate func updateMessageInformation() {
        guard delegate != nil else { return }

        let formatter = DateFormatter()
        formatter.timeStyle = .short

        if let createdAt = message.createdAt {
            labelDate.text = formatter.string(from: createdAt)
        }

        avatarView.imageURL = URL(string: message.avatar)
        avatarView.user = message.user
        labelUsername.text = message.user?.username
        
        // スタンプの場合
        let isStamp = message.internalType == "stamp"
        imageBackGround.isHidden = isStamp
        stampImageView.isHidden = !isStamp

        if isStamp {
            insertStamp(imageView: stampImageView)
            return
        }
        
        let text = NSMutableAttributedString(string: message.textNormalized())

        if self.message.isSystemMessage() {
            text.setFont(MessageTextFontAttributes.italicFont)
            text.setFontColor(MessageTextFontAttributes.systemFontColor)
        } else {
            text.setFont(MessageTextFontAttributes.defaultFont)
            text.setFontColor(MessageTextFontAttributes.defaultFontColor)
        }

        labelText.attributedText = text.transformMarkdown()

        insertGesturesIfNeeded()
        insertAttachments()
    }

    func handleLongPressMessageCell(recognizer: UIGestureRecognizer) {
        delegate?.handleLongPressMessageCell(message, view: contentView, recognizer: recognizer)
    }

}
//
//extension ChatMessageListCell: UIGestureRecognizerDelegate {
//
//    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
//        return false
//    }
//
//}

extension ChatMessageListCell: UITextViewDelegate {

    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        if URL.scheme == "http" || URL.scheme == "https" {
            delegate?.openURL(url: URL)
            return false
        }

        return true
    }
}
