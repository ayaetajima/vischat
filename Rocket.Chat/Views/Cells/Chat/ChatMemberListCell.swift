//
//  ChatMemberListCell.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 7/25/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import UIKit

final class ChatMemberListCell: UITableViewCell {

    static let minimumHeight = CGFloat(55)
    static let identifier = "ChatMemberListCell"

    var user: User! {
        didSet {
            updateUserInformation()
        }
    }

    @IBOutlet weak var userId: UILabel!
    @IBOutlet weak var currentStatusView: UIView!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var avatarViewContainer: UIView! {
        didSet {
            if let avatarView = AvatarView.instantiateFromNib() {
                avatarView.frame = avatarViewContainer.bounds
                avatarViewContainer.layer.cornerRadius = avatarView.frame.size.width/2
                avatarViewContainer.layer.masksToBounds = true

                avatarViewContainer.addSubview(avatarView)
                self.avatarView = avatarView
            }
        }
    }

    weak var avatarView: AvatarView!
    override func prepareForReuse() {
        labelUserName.text = ""
    }

    fileprivate func updateUserInformation() {
        avatarView.user = user
        labelUserName.text = user.name
        userId.text =  "@" + user.username!
        
        switch user.status {
        case .online:
            currentStatusView.backgroundColor = .RCOnline()
            break
        case .busy:
            currentStatusView.backgroundColor = .RCBusy()
            break
        case .away:
            currentStatusView.backgroundColor = .RCAway()
            break
        case .offline:
            currentStatusView.backgroundColor = .RCInvisible()
            break
        }
        
    }
}
