//
//  ChatFileListCell.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 7/25/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol ChatFileListCellProtocol {
    func downloadFile(fileData: JSON)
    func deleteFile(fileData: JSON)
}

final class ChatFileListCell: UITableViewCell {

    static let minimumHeight = CGFloat(55)
    static let identifier = "ChatFileListCell"
    var delegate: ChatFileListCellProtocol!

    var fileData: JSON! {
        didSet {
            updateFileInformation()
        }
    }

    @IBOutlet weak var labelFileName: UILabel!
    @IBOutlet weak var dlButton: UIButton!
    @IBOutlet weak var delButton: UIButton!
    @IBOutlet weak var fileImage: UIImageView!

    weak var avatarView: AvatarView!
    override func prepareForReuse() {
        labelFileName.text = ""
    }

    fileprivate func updateFileInformation() {
        labelFileName.text = fileData["name"].stringValue
    }

    @IBAction func didTapDownloadButton(sender:UIButton) {
        delegate.downloadFile(fileData: fileData)
    }
    
    @IBAction func didTapDeleteButton(sender:UIButton) {
        delegate.deleteFile(fileData: fileData)
    }

}
