//
//  SubscriptionCell.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 8/4/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import UIKit

protocol SubscriptionCellProtocol {
    func didTapIconImage(user:User, recognizer: UITapGestureRecognizer)
}

final class SubscriptionCell: UITableViewCell {

    static let identifier = "CellSubscription"

    internal let labelSelectedTextColor = UIColor(rgb: 0xFFFFFF, alphaVal: 1)
    internal let labelReadTextColor = UIColor(rgb: 0x87CEFA, alphaVal: 1)
    internal let labelUnreadTextColor = UIColor(rgb: 0xFFFFFF, alphaVal: 1)

    var subscription: Subscription! {
        didSet {
            updateSubscriptionInformatin()
        }
    }

    @IBOutlet weak var avatarViewContainer: AvatarView! {
        didSet {
            if let avatarView = AvatarView.instantiateFromNib() {
                avatarView.frame = avatarViewContainer.bounds
                avatarViewContainer.layer.cornerRadius = avatarView.frame.size.width/2
                avatarViewContainer.layer.masksToBounds = true
                
                avatarViewContainer.addSubview(avatarView)
                self.avatarView = avatarView
            }
        }
    }

    weak var avatarView: AvatarView! {
        didSet {
            if self.tapIconGesture == nil {
                let gesture = UITapGestureRecognizer(target: self, action: #selector(iconDidTapped(recognizer:)))
                gesture.delegate = self
                avatarView.addGestureRecognizer(gesture)
                self.tapIconGesture = gesture
            }
        }
    }
    weak var tapIconGesture: UITapGestureRecognizer?
    var delegate: SubscriptionCellProtocol?

    @IBOutlet weak var imageViewIcon: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelUnread: UILabel! {
        didSet {
            labelUnread.layer.cornerRadius = 2
        }
    }

    func updateSubscriptionInformatin() {
        updateIconImage()

        labelName.text = subscription.name

        if subscription.unread > 0 || subscription.alert {
            labelName.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
            labelName.textColor = labelUnreadTextColor
        } else {
            labelName.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)
            labelName.textColor = labelReadTextColor
        }

        labelUnread.alpha = subscription.unread > 0 ? 1 : 0
        labelUnread.text = "\(subscription.unread)"
    }

    func updateIconImage() {
        imageViewIcon.isHidden = true
        avatarViewContainer.isHidden = true

        switch subscription.type {
        case .channel:
            imageViewIcon.image = UIImage(named: "Hashtag")?.imageWithTint(.RCInvisible())
            imageViewIcon.isHidden = false
            break
        case .directMessage:
            if let user = subscription.directMessageUser {
                avatarView.user = user
                avatarViewContainer.isHidden = false
            } else  {
                if subscription.name != "" {
                    let user = User()
                    user.username = subscription.name
                    avatarView.user = user
                    avatarViewContainer.isHidden = false
                }
            }
            break
        case .group:
            imageViewIcon.image = UIImage(named: "Lock")?.imageWithTint(.RCInvisible())
            imageViewIcon.isHidden = false
            break
        }
    }

    // MARK: Gesture method
    
    func iconDidTapped(recognizer: UITapGestureRecognizer) {
        // プロフィール画面に遷移
        delegate?.didTapIconImage(user: self.avatarView.user!, recognizer: recognizer)
    }

}
