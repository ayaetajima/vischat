//
//  AppDelegate.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 7/5/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import SDWebImage

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    // Login設定情報(テスト環境)
//    let host = "chat.tabi.today"
//    let loginURL = "https://app-auth-vis-dev.azurewebsites.net/api/v1/attribute"
//    let loginClientId = "e3fbc134-7791-4d1a-9328-cc1ddf74c5af"
//    let loginClientSecret = "0b69dfd3-8008-4ae2-bfa4-396d79a5fc27"
//    let serverURL = "wss://chat.tabi.today/websocket/"
    //*** Login設定情報(本番環境)
    let host = "et-chat.bizsky.jp"
    let loginURL = "https://auth-vis.bizsky.jp/api/v1/attribute"
    let loginClientId = "72d43570-2a05-4dfb-ac21-fe121203fb76"
    let loginClientSecret = "c19c67b3-400d-49d5-a4cc-88bcac2ff43f"
    let serverURL = "wss://et-chat.bizsky.jp/websocket/"
    
    let loginSchema = "bizsky"
    let loginProductCodes = "0027649273"
    let loginPassword = "password"

    var bizskyID : String?
    var loginAccsessToken :String?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Launcher().prepareToLaunch(with: launchOptions)

        application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil))
        application.registerForRemoteNotifications()

        return true
    }

    // MARK: AppDelegate LifeCycle

    func applicationDidEnterBackground(_ application: UIApplication) {
        if AuthManager.isAuthenticated() != nil {
            UserManager.setUserPresence(status: .away) { (_) in
                SocketManager.disconnect({ (_, _) in })
            }
        }
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if url.scheme == "vis-app" {
            //vis-app://et-chat.bizsky.jp#access_token=＜アクセストークン＞&refresh_token=＜リフレッシュトークン＞&token_type=＜トークンタイプ＞&expires_in=＜有効期限＞&state=＜1意の値＞
            
            // アクセストークンを保存
            if let fragments = url.fragment?.components(separatedBy: "&") {
                let token = fragments[0]
                if token.contains("access_token") {
                    loginAccsessToken = token.replacingOccurrences(of: "access_token=", with: "")
                }
            }
            
            callBizskyAPI()
            
            return true
        }
        
        return GIDSignIn.sharedInstance().handle(
            url,
            sourceApplication: options[.sourceApplication] as? String,
            annotation: options[.annotation]
        )
    }

    // MARK: Login method
    
    func callBizskyAPI() {
        // URLを作成
        let urlString = loginURL + "?client_id=" + loginClientId + "&client_secret=" + loginClientSecret + "&schema=" + loginSchema + "&product_codes=" + loginProductCodes
        
        let request = NSMutableURLRequest(url: URL(string: urlString)!)
        let header = ["Authorization":"bearer " + loginAccsessToken!, "Content-Type":"application/json"]
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = header
        
        // コンフィグを指定してHTTPセッションを生成
        let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: OperationQueue.main)
        
        // HTTP通信を実行する
        // ※dataにJSONデータが入る
        let task:URLSessionDataTask = session.dataTask(with: request as URLRequest, completionHandler: {data, responce, error in
            if error != nil {
                print(error!)
                return
            }
            
            DispatchQueue.main.async {
                // データ取得後の処理
                let json = JSON(data: data!)
                
                // ライセンスチェック
                //licenses の項目があるか→なければエラー
                let licences = json["licences"].array
                if  licences == nil || licences?.count == 0 {
                    self.showAlert(message: "ライセンスの情報に誤りがあります。管理者にお問い合わせください。")
                    return
                }
               
                // licenses 配列に product_code 0027649273 があるか→なければエラー
                var exist = false
                for licence in licences! {
                    if licence["product_code"].string == "0027649273" {
                        // product_code 0027649273 のlicence_status
                        if licence["licence_status"].string == "cancel" {
                            // cancel ならエラー
                            self.showAlert(message: "解約されています。再度ご契約後、ログインを行なってください。")
                            return
                        }
                        if licence["licence_status"].string == "invalid" {
                            // invalid ならエラー
                            self.showAlert(message: "ライセンス情報が間違っているか、不正です。")
                            return
                        }

                        // normal なら次のチェック
                        if licence["licence_status"].string == "normal" {
                            // license_count >= licence_used ならOK
                            let count = licence["licence_count"].intValue
                            let used = licence["licence_used"].intValue
                            if count < used {
                                let message = "ライセンス数が \(count) に対して \(used) 利用されているようです。御社管理者にお問い合わせいただき、ライセンス数の調整をお願いいたします。"
                                self.showAlert(message: message)
                                return
                            }
                        }
                        
                        exist = true
                        break
                    }
                }
                if !exist {
                    self.showAlert(message: "現在のご契約内容ではビジネスチャットはご利用頂けません。管理者にお問い合わせください。")
                    return
                }

                // ログイン処理
                self.authenticateWithUsernameOrEmail(json: json)
            }
            
        })
        
        // HTTP通信を実行
        task.resume()
    }
    
    func showAlert(message: String!) {
        let alert = UIAlertController(title: "", message:message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "閉じる", style: .default, handler: nil))
        if let window = UIApplication.shared.keyWindow {
            window.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    func authenticateWithUsernameOrEmail(json: JSON) {
        let userId = json["userId"].string
        let name = json["name"].string
        let nameSub = json["nameSub"].string
        let tenantId = "t" + json["tenant_id"].string!
        let tenantName = json["tenant_name"].string
        self.bizskyID = json["id"].string

        let encodedName = name?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let encodedNameSub = nameSub?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)

//        let data = tenantId?.data(using: .utf8)
//        let hexTenantId = "t" + (data?.hexString)!

        // ユーザデフォルトに保存
        let userDefaults = UserDefaults.standard
        userDefaults.set(userId, forKey: CookieManager.userDefaultKey_LoginUserId)
        userDefaults.set(encodedName, forKey: CookieManager.userDefaultKey_LoginName)
        userDefaults.set(encodedNameSub, forKey: CookieManager.userDefaultKey_LoginNameSub)
        userDefaults.set(tenantId, forKey: CookieManager.userDefaultKey_LoginTenantId)
        userDefaults.set(tenantName, forKey: CookieManager.userDefaultKey_LoginTenantName)
        userDefaults.synchronize()
                
        // websocket open
        let serverUrl = URL(string: serverURL)
        SocketManager.connect(serverUrl!) { (_, connected) in
            AuthManager.updatePublicSettings(nil) { (settings) in
                if connected {
                    AuthManager.auth(userId!, password: self.loginPassword, completion: self.handleAuthenticationResponse)
                }
            }
        }
    }
    
    func handleAuthenticationResponse(_ response: SocketResponse) {
        if response.isError() {
        }
        else {
            
            // 画像取得用シングルトンのCookieにrc_uidとrc_token追加
            let auth = AuthManager.isAuthenticated()
            let str = "rc_uid=\(auth?.userId ?? "");rc_token=\(auth?.token ?? "");" + CookieManager.getCookieString()
            print("Cookie2:" + str)
            SDWebImageDownloader.shared().setValue(str, forHTTPHeaderField: "Cookie")

            // アバター取得のためのbizskyID登録API
            AuthManager.updateBizskyId(bizskyId: self.bizskyID!, completion: { [weak self] response in
                guard !response.isError() else {
                    return
                }
                
                // MainViewControllerの処理
                if let auth = AuthManager.isAuthenticated() {
                    
                    AuthManager.resume(auth, completion: { [weak self] response in
                        guard !response.isError() else {
                            return
                        }
                        
                        SubscriptionManager.updateSubscriptions(auth, completion: { _ in
                            AuthManager.updatePublicSettings(auth, completion: { _ in
                                
                            })
                            
                            UserManager.userDataChanges()
                            UserManager.changes()
                            SubscriptionManager.changes(auth)
                            
                            if let userIdentifier = auth.userId {
                                PushManager.updateUser(userIdentifier)
                            }
                            
                            // Open chat
                            let storyboardChat = UIStoryboard(name: "Chat", bundle: Bundle.main)
                            let controller = storyboardChat.instantiateInitialViewController()
                            let application = UIApplication.shared
                            
                            if let window = application.keyWindow {
                                window.rootViewController = controller
                            }
                        })
                    })
                }
            })
        }
    }

    // MARK: Remote Notification

    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        Log.debug("Notification: \(notification)")
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        Log.debug("Notification: \(userInfo)")
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        Log.debug("Notification: \(userInfo)")
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        UserDefaults.standard.set(deviceToken.hexString, forKey: PushManager.kDeviceTokenKey)
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        Log.debug("Fail to register for notification: \(error)")
    }

}
