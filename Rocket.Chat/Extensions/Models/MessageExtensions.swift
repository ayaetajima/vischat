//
//  MessageExtensions.swift
//  Rocket.Chat
//
//  Created by Rafael Kellermann Streit on 27/02/17.
//  Copyright © 2017 Rocket.Chat. All rights reserved.
//

import UIKit

extension Message {

    func isSystemMessage() -> Bool {
        return !(
            type == .text ||
            type == .audio ||
            type == .image ||
            type == .video ||
            (type == .textAttachment && internalType != "message_pinned") ||
            type == .url
        )
    }

    // swiftlint:disable function_body_length cyclomatic_complexity
    func textNormalized() -> String {
        let text = Emojione.transform(string: self.text)
        
        var localize = ""
        localize = NSLocale.preferredLanguages.first!
        
        // ピンされた場合、textAttachmentが付いてくるので、internalTypeで判断する
        if internalType == "message_pinned" {
            return localized("chat.message.type.message_pinned")
        }
        
        switch type {
            case .roomNameChanged:
                if localize.contains("en") {
                    //英語の時の処理
                    return String(
                        format: localized("chat.message.type.room_name_changed"),
                        text,
                        self.user?.username ?? ""
                    )
                } else {
                    //日本語,Baseの時の処理
                    return String(
                        format: localized("chat.message.type.room_name_changed"),
                        self.user?.username ?? "",
                        text
                    )
                }

            case .userAdded:
                if localize.contains("en") {
                    return String(
                        format: localized("chat.message.type.user_added_by"),
                        text,
                        self.user?.username ?? ""
                    )
                } else {
                    return String(
                        format: localized("chat.message.type.user_added_by"),
                        self.user?.username ?? "",
                        text
                    )
                }


            case .userRemoved:
                if localize.contains("en") {
                    return String(
                        format: localized("chat.message.type.user_removed_by"),
                        text,
                        self.user?.username ?? ""
                    )
                } else {
                    return String(
                        format: localized("chat.message.type.user_removed_by"),
                        self.user?.username ?? "",
                        text
                    )
                }

            case .userJoined:
                return localized("chat.message.type.user_joined")

            case .userLeft:
                return String(
                    format: localized("chat.message.type.user_left"),
                    self.user?.username ?? ""
                )


            case .userMuted:
                return String(
                    format: localized("chat.message.type.user_muted"),
                    text,
                    self.user?.username ?? ""
                )

            case .userUnmuted:
                return String(
                    format: localized("chat.message.type.user_unmuted"),
                    text,
                    self.user?.username ?? ""
                )

            case .welcome:
                return String(
                    format: localized("chat.message.type.welcome"),
                    text
                )

            case .messageRemoved:
                return localized("chat.message.type.message_removed")
            
            // ピンされた場合、textAttachmentが付いてくるので、ここには来ない
//            case .messagePinned:
//                return localized("chat.message.type.message_pinned")

            case .subscriptionRoleAdded:
                return String(
                    format: localized("chat.message.type.subscription_role_added"),
                    text,
                    role,
                    self.user?.username ?? ""
                )

            case .subscriptionRoleRemoved:
                return String(
                    format: localized("chat.message.type.subscription_role_removed"),
                    text,
                    role,
                    self.user?.username ?? ""
                )

            case .roomArchived:
                return String(
                    format: localized("chat.message.type.room_archived"),
                    text
                )

            case .roomUnarchived:
                return String(
                    format: localized("chat.message.type.room_unarchived"),
                    text
                )
            
            case .roomChangedTopic:
                return String(
                    format: localized("chat.message.type.room_changed_topic"),
                    text,
                    self.user?.username ?? ""
                )
            case .roomChangedDescription:
                return String(
                    format: localized("chat.message.type.room_changed_description"),
                    text,
                    self.user?.username ?? ""
                )

            default: break
        }

        return text
    }

}
