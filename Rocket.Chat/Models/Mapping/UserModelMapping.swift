//
//  UserModelMapping.swift
//  Rocket.Chat
//
//  Created by Rafael Kellermann Streit on 13/01/17.
//  Copyright © 2017 Rocket.Chat. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

extension User: ModelMappeable {
    func map(_ values: JSON) {
        if self.identifier == nil {
            self.identifier = values["_id"].string
        }

        if let username = values["username"].string {
            self.username = username
        }

        if let name = values["name"].string {
            self.name = name
        }

        if let deptName = values["dept_name"].string {
            self.deptName = deptName
        }

        if let roles = values["roles"].array {
            self.roles = roles[0].stringValue            
        }
        
        if let status = values["status"].string {
            self.status = UserStatus(rawValue: status) ?? .offline
        }
        
        if let lastLogin = values["lastLogin"]["$date"].double {
            self.lastLogin = Date.dateFromInterval(lastLogin)
        }
        
        if let emails = values["emails"].array {
            self.emails = List<Email>()
            
            for email in emails {
                let obj = Email()
                obj.email = email["address"].stringValue
                obj.verified = email["verified"].boolValue
                
                self.emails.append(obj)
            }
        }
    }
}
