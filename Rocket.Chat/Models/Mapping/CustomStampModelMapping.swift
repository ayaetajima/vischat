//
//  CustomStampModelMapping.swift
//  Rocket.Chat
//
//  Created by Rafael Kellermann Streit on 13/01/17.
//  Copyright © 2017 Rocket.Chat. All rights reserved.
//

import Foundation
import SwiftyJSON

extension CustomStamp: ModelMappeable {
    func map(_ values: JSON) {
        if self.identifier == nil {
            self.identifier = values["_id"].string
        }

        if let name = values["name"].string {
            self.name = name
        }

        if let aliases = values["aliases"].array {
            if aliases.count > 0{
                self.alias = aliases[0].stringValue
            }
        }
        
        if let ext = values["extension"].string {
            self.ext = ext
        }
        
        if let updatedAt = values["_updatedAt"]["$date"].double {
            self.updatedAt = Date.dateFromInterval(updatedAt)
        }
    }
}
