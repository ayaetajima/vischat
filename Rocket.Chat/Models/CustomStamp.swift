//
//  CustomStamp.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 7/7/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON


class CustomStamp: BaseModel {
    dynamic var name = ""
    dynamic var alias = ""
    dynamic var ext = ""
    dynamic var updatedAt: Date?

    fileprivate dynamic var privateStatus = UserStatus.offline.rawValue
    var status: UserStatus {
        get { return UserStatus(rawValue: privateStatus) ?? UserStatus.offline }
        set { privateStatus = newValue.rawValue }
    }
}

extension CustomStamp {
    
    fileprivate static func fullURLWith(_ path: String?) -> URL? {
        guard let path = path?.replacingOccurrences(of: "//", with: "/") else { return nil }
        guard let auth = AuthManager.isAuthenticated() else { return nil }
        guard let userId = auth.userId else { return nil }
        guard let token = auth.token else { return nil }
        guard let baseURL = auth.baseURL() else { return nil }
        let urlString = "\(baseURL)\(path)?rc_uid=\(userId)&rc_token=\(token)"
        return URL(string: urlString)
    }
    
    func fullImageURL() -> URL? {
        var allowedCharacterSet = CharacterSet.alphanumerics
        allowedCharacterSet.insert(charactersIn: "-._~")
        let encodedString = self.name.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet)

        let imageURL = "/emoji-custom/" + encodedString! + "." + self.ext
        
        return CustomStamp.fullURLWith(imageURL)
    }
    
}
