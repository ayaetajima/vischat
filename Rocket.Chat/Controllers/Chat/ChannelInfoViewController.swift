//
//  ChannelInfoViewController.swift
//  Rocket.Chat
//
//  Created by Rafael Kellermann Streit on 09/03/17.
//  Copyright © 2017 Rocket.Chat. All rights reserved.
//

import UIKit

protocol ChannelInfoViewControllerDelegate {
    func didChangeChannelName(newName:String)
}

class ChannelInfoViewController: BaseViewController {

    var delegate: ChannelInfoViewControllerDelegate?
    var tableViewData: [[Any]] = [] {
        didSet {
            tableView?.reloadData()
        }
    }

    var subscription: Subscription! {
        didSet {
            // その他の操作の部分を作成
            let channelInfoData = [
                ChannelInfoDetailCellData(title: localized("chat.info.item.leave_channel"), detail: "")
            ]

            if subscription.type == .directMessage {
                tableViewData = [[
                    ChannelInfoUserCellData(user: subscription.directMessageUser)
                ], channelInfoData]
            }
            else {
                let topic = subscription.roomTopic?.characters.count ?? 0 == 0 ? localized("chat.info.item.no_topic") : subscription.roomTopic
                let description = subscription.roomDescription?.characters.count ?? 0 == 0 ? localized("chat.info.item.no_description") : subscription.roomDescription

                // tableDataに追加していく
                tableViewData = [
                    // グループ名
                    [ChannelInfoBasicCellData(title:subscription.name)],
                    // 概要
                    [ChannelInfoDescriptionCellData(title:"",description:description)],
                    // トピック
                    [ChannelInfoDescriptionCellData(title:"",description:topic)],
                    // その他の操作
                    channelInfoData]
            }
        }
    }

    @IBOutlet weak var tableView: UITableView!

    var editBarButton: UIBarButtonItem!
    var saveBarButton: UIBarButtonItem!
    var editable: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = localized("chat.info.title")
        
        if subscription.type != .directMessage {
            editBarButton = UIBarButtonItem.init(title: "編集", style: .plain, target: self, action: #selector(didTapEditButton))
            
            saveBarButton = UIBarButtonItem.init(title: "保存", style: .done, target: self, action:
                #selector(didTapSaveButton))
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.rightBarButtonItem = editBarButton
        editable = false
    }


    // MARK: IBAction
    @IBAction func buttonCloseDidPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    func didTapEditButton() {
        self.navigationItem.rightBarButtonItem = saveBarButton
        editable = true
        tableView?.reloadData()
    }

    func didTapSaveButton() {
        self.navigationItem.rightBarButtonItem = editBarButton
        editable = false
        
        // 設定を文字列に変換
        var newSettings = self.tableViewData
        let count = self.tableViewData.count
        var newName:String!
        var newDescription:String!
        var newTopic:String!
        
        for i in 0..<count
        {
            let cell = self.tableView.cellForRow(at: IndexPath(item: 0, section: i))
            if (cell as? ChannelInfoBasicCell) != nil {
                let basicCell = cell as! ChannelInfoBasicCell
                var newData = ChannelInfoBasicCellData()
                newData.title = basicCell.textTitle.text
                
                newSettings[i] = [newData]
                newName = basicCell.textTitle.text!
                
                if newName.contains(" ") || newName.contains("　") {
                    let alert = UIAlertController(title: "", message: "グループの設定変更に失敗しました", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: localized("global.ok"), style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return
                }
            }
            
            if (cell as? ChannelInfoDetailCell) != nil {
                // 現状は使用なし
            }
            
            if (cell as? ChannelInfoDescriptionCell) != nil {
                let descriptionCell = cell as! ChannelInfoDescriptionCell
                var newData = ChannelInfoDescriptionCellData()
                newData.description = descriptionCell.textDescription.text
                
                newSettings[i] = [newData]
                if i == 1 {
                    newDescription = descriptionCell.textDescription.text!
                }else if i == 2 {
                    newTopic = descriptionCell.textDescription.text!
                }
            }
        }
        
        // グループ情報を保存(API)
        for i in 0..<3
        {
            if i==0 {
                if subscription.name != newName {
                    SubscriptionManager.saveRoomSettings(self.subscription.rid, settings: ["roomName",newName], completion:{_ in
                        self.delegate?.didChangeChannelName(newName: newName)
                    })
                }
            }
            else if i==1 {
                if subscription.roomDescription != newDescription {
                    if subscription.roomDescription == localized("chat.info.item.no_description") && newDescription == "" {
                        continue
                    }
                    SubscriptionManager.saveRoomSettings(self.subscription.rid, settings: ["roomDescription",newDescription], completion:{response in
                        print(response)
                    })
                }
            }
            else if i==2 {
                if subscription.roomTopic != newTopic {
                    if subscription.roomTopic == localized("chat.info.item.no_topic") && newTopic == "" {
                        continue
                    }
                    SubscriptionManager.saveRoomSettings(self.subscription.rid, settings: ["roomTopic",newTopic], completion:{ _ in
                        // 保持しているデータを更新
                        self.tableViewData = newSettings
                        if let auth = AuthManager.isAuthenticated() {
                            SubscriptionManager.updateSubscriptions(auth, completion: { _ in})
                        }
                        // 再表示
                        self.tableView?.reloadData()
                    })
                }
            }
        }
    }
}


// MARK: UITableViewDelegate

extension ChannelInfoViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = tableViewData[indexPath.section][indexPath.row]

        if let data = data as? ChannelInfoBasicCellData {
            if let cell = tableView.dequeueReusableCell(withIdentifier: ChannelInfoBasicCell.identifier) as? ChannelInfoBasicCell {
                cell.data = data
                
                // adminユーザのみ編集可能
                let user = AuthManager.currentUser()!
                let roles = user.roles
                if roles == "admin" {
                    cell.textTitle.isUserInteractionEnabled = editable
                }else {
                    cell.textTitle.isUserInteractionEnabled = false
                }
                return cell
            }
        }

        if let data = data as? ChannelInfoDetailCellData {
            if let cell = tableView.dequeueReusableCell(withIdentifier: ChannelInfoDetailCell.identifier) as? ChannelInfoDetailCell {
                cell.data = data
                cell.labelTitle.isUserInteractionEnabled = editable
                cell.labelDetail.isUserInteractionEnabled = editable
                return cell
            }
        }

        if let data = data as? ChannelInfoUserCellData {
            if let cell = tableView.dequeueReusableCell(withIdentifier: ChannelInfoUserCell.identifier) as? ChannelInfoUserCell {
                cell.data = data
                return cell
            }
        }

        if let data = data as? ChannelInfoDescriptionCellData {
            if let cell = tableView.dequeueReusableCell(withIdentifier: ChannelInfoDescriptionCell.identifier) as? ChannelInfoDescriptionCell {
                cell.data = data
                if indexPath.section == 1 {
                    // adminユーザのみ編集可能
                    let user = AuthManager.currentUser()!
                    let roles = user.roles
                    if roles == "admin" {
                        cell.textDescription.isEditable = editable
                    }else {
                        cell.textDescription.isEditable = false
                    }
                }else {
                    cell.textDescription.isEditable = editable
                }
                return cell
            }
        }

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let data = tableViewData[indexPath.section][indexPath.row]

        if data is ChannelInfoBasicCellData {
            return ChannelInfoBasicCell.defaultHeight
        }
        else if data is ChannelInfoDescriptionCellData {
            return ChannelInfoDescriptionCell.defaultHeight
        }
        else if data is ChannelInfoDetailCellData {
            return ChannelInfoDetailCell.defaultHeight
        }
        else if data is ChannelInfoUserCellData {
            return ChannelInfoUserCell.defaultHeight
        }
        else {
            return 44
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = tableViewData[indexPath.section][indexPath.row]

        if data as? ChannelInfoDetailCellData != nil {
            
            if(data as! ChannelInfoDetailCellData).title == localized("chat.info.item.leave_channel"){
                let alert = UIAlertController(title: nil, message: "退出しますか？", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: localized("global.ok"), style: .default, handler: { (_) in
                    // グループから退出
                    SubscriptionManager.leaveRoom(room: self.subscription.rid) { _ in
                        self.dismiss(animated: true, completion: nil)
                    }
                }))
                alert.addAction(UIAlertAction(title: localized("global.cancel"), style: .cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
            }

            if let selectedIndex = tableView.indexPathForSelectedRow {
                tableView.deselectRow(at: selectedIndex, animated: true)
            }
        }
    }

}

// MARK: UITableViewDataSource

extension ChannelInfoViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewData.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewData[section].count
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        let view = UIView(frame:CGRect(x:0, y:0, width:tableView.frame.width, height:30))
        view.backgroundColor = UIColor.clear
        
        let label = UILabel(frame:CGRect(x:10, y:3, width:view.frame.width - 20, height:view.frame.height - 6))
        label.textColor = UIColor.gray
        label.font = UIFont.boldSystemFont(ofSize: 14)
        view.addSubview(label)
        
        var str = ""
        switch section {
        case 0:
            str = localized("chat.info.item.channel_name")
        case 1:
            str = localized("chat.info.item.description")
        case 2:
            str = localized("chat.info.item.topic")
        case 3:
            str = localized("chat.info.item.other_menu")
        default:
            str = ""
        }
        label.text = str
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
}
