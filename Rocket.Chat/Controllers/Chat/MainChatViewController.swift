//
//  MainChatViewController.swift
//  Rocket.Chat
//
//  Created by Rafael Kellermann Streit on 15/03/17.
//  Copyright © 2017 Rocket.Chat. All rights reserved.
//

import UIKit
import SideMenuController

class MainChatViewController: SideMenuController {

    class func shared() -> MainChatViewController? {
        return UIApplication.shared.windows.first?.rootViewController as? MainChatViewController
    }

    class func closeSideMenuIfNeeded() {
        if let instance = shared() {
            if instance.sidePanelVisible {
                instance.toggle()
            }
        }
    }

    required init?(coder aDecoder: NSCoder) {
        SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "left_menu")
        SideMenuController.preferences.drawing.sidePanelPosition = .underCenterPanelLeft
        SideMenuController.preferences.drawing.sidePanelWidth = 280
        SideMenuController.preferences.drawing.centerPanelShadow = true
        SideMenuController.preferences.interaction.swipingEnabled = true
        SideMenuController.preferences.interaction.panningEnabled = true
        SideMenuController.preferences.animating.statusBarBehaviour = .horizontalPan

        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        super.delegate = self
        
        performSegue(withIdentifier: "showCenterController", sender: nil)
        performSegue(withIdentifier: "containSideMenu", sender: nil)
    }
}

extension MainChatViewController: SideMenuControllerDelegate {
    func sideMenuControllerDidHide(_ sideMenuController: SideMenuController) {
        if sideMenuController.sideViewController.isMember(of: UINavigationController.self) {
            let navi = sideMenuController.sideViewController as! UINavigationController
            let sub = navi.topViewController as! SubscriptionsViewController
            sub.searchView.textFieldSearch.resignFirstResponder()
        }
    }
    
    func sideMenuControllerDidReveal(_ sideMenuController: SideMenuController) {
        if sideMenuController.sideViewController.isMember(of: UINavigationController.self) {
            let navi = sideMenuController.sideViewController as! UINavigationController
            let sub = navi.topViewController as! SubscriptionsViewController
            sub.loadLogoImage()
        }
    }
    
}
