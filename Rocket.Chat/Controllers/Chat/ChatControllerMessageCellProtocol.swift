//
//  ChatControllerMessageCellProtocol.swift
//  Rocket.Chat
//
//  Created by Rafael Kellermann Streit on 17/12/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import UIKit
import SafariServices
import MobilePlayer

extension ChatViewController: ChatMessageCellProtocol {
    func didTapIconImage(_ message: Message, view: UIView, recognizer: UITapGestureRecognizer) {
        // プロフィール画面へ遷移
        self.performSegue(withIdentifier: "User Profile", sender: message.user)
    }
    
    func didTapTextMessage(_ message: Message, view: UIView, recognizer: UITapGestureRecognizer) {
        // メッセージ詳細へ遷移
        self.performSegue(withIdentifier: "Message Detail", sender: message)
    }
    
    func didLongTapTextMessage(_ message: Message, view: UIView, recognizer: UILongPressGestureRecognizer) {
        // 編集ボタンを表示
        let alert = UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "編集", style: .default, handler: {_ in
            self.textView.text = message.text
            self.textView.becomeFirstResponder()
        }))
        alert.addAction(UIAlertAction(title: "キャンセル", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
        
        // 編集フラグを立てる
        self.messageForUpdate = message
    }
    
    func openURL(url: URL) {
        let controller = SFSafariViewController(url: url)
        present(controller, animated: true, completion: nil)
    }

    func openURLFromCell(url: MessageURL) {
        guard let targetURL = url.targetURL else { return }
        guard let destinyURL = URL(string: targetURL) else { return }
        let controller = SFSafariViewController(url: destinyURL)
        present(controller, animated: true, completion: nil)
    }

    func openVideoFromCell(attachment: Attachment) {
        guard let videoURL = attachment.fullVideoURL() else { return }
        let controller = MobilePlayerViewController(contentURL: videoURL)
        controller.title = attachment.title
        controller.activityItems = [attachment.title, videoURL]
        present(controller, animated: true, completion: nil)
    }

    func openImageFromCell(attachment: Attachment, thumbnail: UIImageView) {
        if let image = thumbnail.image {
            mediaFocusViewController.show(image, from: thumbnail)
        } else {
            mediaFocusViewController.showImage(from: attachment.fullImageURL(), from: thumbnail)
        }
    }

    func viewDidCollpaseChange(view: UIView) {
        guard let origin = collectionView?.convert(CGPoint.zero, from: view) else { return }
        guard let indexPath = collectionView?.indexPathForItem(at: origin) else { return }
        collectionView?.reloadItems(at: [indexPath])
    }

}
