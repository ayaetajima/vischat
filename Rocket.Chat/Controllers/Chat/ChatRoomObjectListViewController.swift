//
//  ChatRoomObjectListViewController.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 10/10/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import NVActivityIndicatorView

enum ObjectType: Int {
    case roomMember, file, reader
}


final class ChatRoomObjectListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var inviteButton: UIButton!
    
    var dataController = ChatDataController()
    var subscription: Subscription!
    var userList: List<User>?
    var fileList = [JSON]()
    var readerNameList = [String]()
    var objectType: ObjectType!
    weak var viewDMSuggest: SubscriptionDMSuggestView?

    @IBAction func didTapCloseBarButton(sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        if objectType == .roomMember {
            // 招待ボタン
            tableView.frame.size.height = self.view.frame.height - inviteButton.frame.height
            inviteButton.isHidden = false
        }
        else {
            inviteButton.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if objectType == .roomMember {
            if userList != nil {
                if userList!.count > 0 {
                    return
                }
            }
            
            // API：メンバーリスト
            updateRoomMemberList()
        } else if objectType == .file {
            // API:ファイル一覧
            SubscriptionManager.getFileList(room: subscription.rid, completion: {(list)in
                self.fileList = list
                self.tableView.reloadData()
            })
        } else if objectType == .reader {
            // 既読メンバー
            if readerNameList.count == 0 {
                return
            }

            UserManager.getUserInformationByUsernames(usernames: readerNameList, completion: {(list) in
                let newList = List<User>()
                for json in list {
                    let member = User()
                    member.username = json["username"].stringValue
                    member.name = json["name"].stringValue
                    member.identifier = json["_id"].stringValue
                    member.status = UserStatus(rawValue: json["status"].stringValue)!
                    newList.append(member)
                }
                
                self.userList = newList
                self.tableView.reloadData()
            })
        }

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // データの受け渡しUserで統一
        if segue.identifier == "User Profile" {
            let controller = segue.destination as! UserProfileViewController
            controller.user = sender as! User
        }
    }
    
    func updateRoomMemberList() {
        SubscriptionManager.getUsersOfRoom(room: subscription.rid, completion: {(list) in
            for json in list {
                let member = User()
                member.username = json["username"].stringValue
                member.name = json["name"].stringValue
                member.identifier = json["_id"].stringValue
                member.status = UserStatus(rawValue: json["status"].stringValue)!
                
                if self.userList == nil {
                    self.userList = List<User>()
                }
                self.userList?.append(member)
            }
            self.tableView.reloadData()
        })
    }
    
    // MARK: Button action
    @IBAction func didTapInviteButton(sender: UIButton) {
        // サジェスト画面
        guard let suggestView = SubscriptionDMSuggestView.instantiateFromNib() else {
            return }
        
        suggestView.delegate = self
        suggestView.searchType = .inviteRoom
        suggestView.roomMember = userList
        suggestView.frame.size.height = self.view.frame.height
        UIApplication.shared.keyWindow?.addSubview(suggestView)
        self.viewDMSuggest = suggestView
    }
}

extension ChatRoomObjectListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if objectType == .file {
            return fileList.count
        }
        
        if userList != nil {
            return userList!.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if objectType == .file {
            return ChatFileListCell.minimumHeight
        }
        return ChatMemberListCell.minimumHeight
    }
}

extension ChatRoomObjectListViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if objectType == .file {
            if let cell = tableView.dequeueReusableCell(withIdentifier: ChatFileListCell.identifier) as? ChatFileListCell {
                cell.fileData = fileList[indexPath.row]
                
                return cell
            }
            else {
                let cell = ChatFileListCell.instantiateFromNib()
                cell?.delegate = self
                cell?.fileData = fileList[indexPath.row]
                
                return cell!
            }
        }
        else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: ChatMemberListCell.identifier) as? ChatMemberListCell {
                cell.user = userList?[indexPath.row]
                
                if objectType == .reader {
                    cell.isUserInteractionEnabled = false
                }else {
                    cell.isUserInteractionEnabled = true
                }
                
                return cell
            }
            else {
                let cell = ChatMemberListCell.instantiateFromNib()
                cell?.user = userList?[indexPath.row]

                if objectType == .reader {
                    cell?.isUserInteractionEnabled = false
                }else {
                    cell?.isUserInteractionEnabled = true
                }

                return cell!
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if objectType != .roomMember {
            return
        }
        
        let sender = userList?[indexPath.row]
        performSegue(withIdentifier: "User Profile", sender: sender)
    }
    
    func getDownloadFile(fileName :String , fileId: String) {
        
        // loading表示
        DispatchQueue.main.async(execute: {
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(
                message: "Loading",
                type: .ballPulse
            ))
        });
        
        // ドメイン,uid,toke等取得
        let domain = (subscription.auth?.settings?.siteURL)!
        
        let uid = (subscription.auth?.userId)!
        let token = (subscription.auth?.token)!
        var urlString : String = ""
        
        // ダウンロードURLを作成
        urlString = domain + "/file-upload/" + fileId + "/" + fileName
        // URLエンコード
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
        
        let request = NSMutableURLRequest(url: NSURL(string: encodedURL!)! as URL)

        // cookieを作成
        let cookie = CookieManager.getCookieString()
        let header = ["Cookie":"rc_uid=" + uid + ";rc_token=" + token + ";" + cookie]
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = header
        
        // use NSURLSessionDataTask
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { dData, response, error in
            if (error == nil) {
                do {
                    // ダウンロードできたら保存
                    if let data = dData {
                        let filePath = self.getSaveDirectory() + fileName
                        let fileURL = URL(fileURLWithPath: filePath)
                        try data.write(to: fileURL, options: .atomic)
                    }
                    // 保存後表示
                    self.showDownloadView(url: self.getSaveDirectory() + fileName , fileName: fileName)
                } catch let error as NSError {
                    print("download error: \(error)")
                }
            } else {
                print("download error: \(String(describing: error))")
            }
            DispatchQueue.main.async(execute: {
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            });
            
        })
        task.resume()

    }
    
    
    // 保存するディレクトリのパス
    func getSaveDirectory() -> String {
        let cachesDirPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).last!
        return cachesDirPath + "/"
    }
    
    func showDownloadView(url :String ,fileName :String) {
        DispatchQueue.main.async(execute: {
            // Main Threadで実行する
            let downloadView:DownloadFileViewController = self.storyboard!.instantiateViewController(withIdentifier: "DownloadFileView") as! DownloadFileViewController
            
            downloadView.loadFileURL(url: url, title:fileName)
            // モーダルウィンドウを呼び出します
            self.present(downloadView as UIViewController, animated: true, completion:nil)
        });
    }
}

extension ChatRoomObjectListViewController: ChatFileListCellProtocol
{
    func downloadFile(fileData: JSON) {
        getDownloadFile(fileName: fileData["name"].stringValue , fileId : fileData["_id"].stringValue)
        
    }
    
    func deleteFile(fileData: JSON) {
        var message: String!
        var canDelete = false

        let fileName = fileData["_id"].stringValue
        
        let user = AuthManager.currentUser()!
        let roles = user.roles
        if roles == "admin" {
            // 全ファイル削除可能
            message = "このファイルを削除してよろしいですか？"
            canDelete = true
        }else if roles == "user" {
            // 自分のファイルのみ削除可能
            if fileData["userId"].stringValue == user.identifier {
                message = "このファイルを削除してよろしいですか？"
                canDelete = true
            }else{
                message = "自分以外のファイルは削除できません"
            }
        }else {
            // 削除不可(botなど)
            message = "ファイルを削除できないメンバーです"
        }
        
        let alert = UIAlertController(
            title: "ファイルの削除",
            message: message,
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {_ in
            self.commitDelete(fileName: fileName)
        }))
        
        if canDelete {
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {_ in
            }))
        }

        present(alert, animated: true, completion: nil)
    }
    
    func commitDelete(fileName:String) {

        SubscriptionManager.deleteFile(fileName: fileName, completion: {
            SubscriptionManager.getFileList(room: self.subscription.rid, completion: {(list)in
                self.fileList = list
                self.tableView.reloadData()
            })
        })
    }
}

extension ChatRoomObjectListViewController: SubscriptionDMSuggestViewProtocol {
    func didSelect(subscription: Subscription) {
        // メンバー招待API
        SubscriptionManager.addUserToRoom(room: self.subscription.rid, username: subscription.name, completion: {_ in
            self.viewDMSuggest?.removeFromSuperview()
            
            // リスト更新
            self.userList?.removeAll()
            self.updateRoomMemberList()
        })
    }
    
    func closeSuggestView() {
        self.viewDMSuggest?.removeFromSuperview()
    }
}
