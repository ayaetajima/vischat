
//  ChatViewController.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 7/21/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import RealmSwift
import SlackTextViewController
import URBMediaFocusViewController
import NVActivityIndicatorView
import SideMenuController

// swiftlint:disable file_length type_body_length
final class ChatViewController: SLKTextViewController {
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    var buttonEmoji: UIButton!
    var emojiOriginX: CGFloat!
    var buttonEmojiFrame: CGRect!
    var textViewWidth: CGFloat!
    var sendButton: UIButton!
    var uploadButton: UIButton!
    
    var activityIndicator: NVActivityIndicatorView!
    @IBOutlet weak var activityIndicatorContainer: UIView! {
        didSet {
            let width = activityIndicatorContainer.bounds.width
            let height = activityIndicatorContainer.bounds.height
            let frame = CGRect(x: 0, y: 0, width: width, height: height)
            let activityIndicator = NVActivityIndicatorView(
                frame: frame,
                type: .ballPulse,
                color: .RCDarkBlue(),
                padding: 0
            )
            
            activityIndicatorContainer.addSubview(activityIndicator)
            self.activityIndicator = activityIndicator
        }
    }
    
    @IBOutlet weak var buttonScrollToBottom: UIButton!
    var buttonScrollToBottomMarginConstraint: NSLayoutConstraint?
    
    weak var chatTitleView: ChatTitleView?
    weak var profileView: ChatProfileView?
    var isShowProfile = false
    weak var emojiView: ChatEmojiView?
    weak var chatPreviewModeView: ChatPreviewModeView?
    weak var chatHeaderViewOffline: ChatHeaderViewOffline?
    lazy var mediaFocusViewController = URBMediaFocusViewController()
    
    var dataController = ChatDataController()
    
    var searchResult: [String: Any] = [:]
    var needTargetJump = false
    var targetMessage: Message? // ジャンプする先のメッセージ
    
    var closeSidebarAfterSubscriptionUpdate = false
    
    var messageForUpdate: Message? // 編集用に選択されたメッセージ
    
    var isRequestingHistory = false
    let socketHandlerToken = String.random(5)
    var messagesToken: NotificationToken!
    var messages: Results<Message>!
    var subscription: Subscription! {
        didSet {
            updateSubscriptionInfo()
            markAsRead()
        }
    }
    
    weak var viewUserMenu: SubscriptionUserStatusView?
    
    weak var searchHeaderView: ChatHeaderSearchView!
    var tempChatData = [ChatData]()
    
    var keyboardFrame = CGRect.zero
    
    // MARK: View Life Cycle
    
    class func sharedInstance() -> ChatViewController? {
        if let main = UIApplication.shared.delegate?.window??.rootViewController as? MainChatViewController {
            
            if let nav = main.centerViewController as? UINavigationController {
                return nav.viewControllers.first as? ChatViewController
            }
        }
        
        return nil
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        SocketManager.removeConnectionHandler(token: socketHandlerToken)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        registerCells()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.tintColor = UIColor(rgb: 0x5B5B5B, alphaVal: 1)
        
        mediaFocusViewController.shouldDismissOnTap = true
        mediaFocusViewController.shouldShowPhotoActions = true
        
        isInverted = false
        bounces = true
        shakeToClearEnabled = true
        isKeyboardPanningEnabled = true
        shouldScrollToBottomAfterKeyboardShows = false
        
        leftButton.setImage(UIImage(named: "Upload"), for: .normal)
        
        rightButton.isEnabled = false
        
        setupTitleView()
        setupTextViewSettings()
        setupScrollToBottomButton()
        
        // TODO: this should really goes into the view model, when we have it
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.reconnect), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        SocketManager.addConnectionHandler(token: socketHandlerToken, handler: self)
        
        guard let auth = AuthManager.isAuthenticated() else { return }
        let subscriptions = auth.subscriptions.sorted(byKeyPath: "lastSeen", ascending: false)
        if let subscription = subscriptions.first {
            self.subscription = subscription
        }
        
        view.bringSubview(toFront: activityIndicatorContainer)
        view.bringSubview(toFront: buttonScrollToBottom)
        view.bringSubview(toFront: textInputbar)
        
        // スワイプジェスチャー
        let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(didTapProfileButton))
        swipeGesture.direction = UISwipeGestureRecognizerDirection.left
        swipeGesture.numberOfTouchesRequired = 1
        
        let rightView = UIView(frame:CGRect(x:self.view.frame.width - 20, y:0, width:20, height:self.view.frame.height))
        rightView.backgroundColor = UIColor.clear
        rightView.addGestureRecognizer(swipeGesture)
        
        self.view.addSubview(rightView)
        
        // プロフィール生成
        profileView = ChatProfileView.instantiateFromNib()
        profileView?.frame = CGRect(x: self.view.frame.size.width, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        profileView?.delegate = self
        
        // 閉じるジェスチャー
        let closeGesture = UITapGestureRecognizer(target: self, action: #selector(closeProfileView))
        profileView?.clearView.addGestureRecognizer(closeGesture)
        
        self.view.addSubview(profileView!)
        
        // 絵文字ボタンを配置
        buttonEmoji = UIButton()
        buttonEmoji.setImage(UIImage(named:"status"), for: UIControlState.normal)
        buttonEmoji.addTarget(self, action:#selector(didTapEmojiButton), for: .touchUpInside)
        self.textInputbar.addSubview(buttonEmoji)
        
        // 送信ボタンを画像に変更（左側に透明な画像をはめて、その上に絵文字ボタンを置いています）
        let image = UIImage(named: "send2.png")
        textInputbar.rightButton.setImage(image, for: .normal)
        initTextViewMargins()
        
        textInputbar.rightButton.setTitle("", for: .normal)
        textInputbar.rightButton.frame = CGRect(x:emojiOriginX + 60, y:self.textInputbar.textView.frame.size.height - 18, width: 45.0, height: 25.0)
        
    }
    
    internal func reconnect() {
        if !SocketManager.isConnected() {
            SocketManager.reconnect()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let auth = AuthManager.isAuthenticated() else { return }
        guard let subscription = self.subscription else { return }
        
        //*** 絵文字画面生成(subscription取ってから) viewDidLoadにもどす？
        if emojiView == nil {
            emojiView = ChatEmojiView.instantiateFromNib()
            emojiView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height-44)
            emojiView?.isHidden = true
            emojiView?.delegate = self
            
            emojiView?.serverURL = (subscription.auth?.serverURL)!
            self.view.addSubview(emojiView!)
        }
        
        // スタンプの取得（初回生成）
        MessageManager.getListEmojiCustom({(list) in
            self.emojiView?.stampList = list
            self.emojiView?.setEmojiCategories()
        })
        
        view?.layoutSubviews()
        
        // キーボードの通知追加
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(_:)),
            name: NSNotification.Name.UIKeyboardWillShow,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: NSNotification.Name.UIKeyboardWillHide,
            object: nil
        )
        
        // デリゲート追加
        chatTitleView?.delegate = self
        
        // メンバー情報更新
        updateCurrentUserInformation()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let insets = UIEdgeInsets(
            top: 0,
            left: 0,
            bottom: chatPreviewModeView?.frame.height ?? 0,
            right: 0
        )
        
        collectionView?.contentInset = insets
        collectionView?.scrollIndicatorInsets = insets
        
        initTextViewMargins()
        buttonEmoji.frame = CGRect(x:emojiOriginX, y:self.textInputbar.rightButton.frame.origin.y, width: 20.0, height: 20.0)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: nil, completion: { _ in
            self.collectionView?.collectionViewLayout.invalidateLayout()
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        textView.resignFirstResponder()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nav = segue.destination as? UINavigationController, segue.identifier == "Channel Info" {
            if let controller = nav.viewControllers.first as? ChannelInfoViewController {
                controller.subscription = self.subscription
                controller.delegate = self
            }
        }
        else if segue.identifier == "Message List" {
            if let controller = segue.destination as? ChatMessageListViewController {
                controller.title = sender as?String
                controller.subscription = subscription
            }
        }
        else if segue.identifier == "Message Detail" {
            if let controller = segue.destination as? ChatMessageDetailController {
                controller.subscription = subscription
                controller.message = sender as! Message
                controller.delegate = self
            }
        }
        else if segue.identifier == "Searched List" {
            if let controller = segue.destination as? ChatMessageListViewController {
                controller.title = "検索結果"
                controller.delegate = self
                controller.subscription = subscription
                controller.messageList = (sender as? List<Message>)!
            }
        }
        else if segue.identifier == "Member List" {
            if let controller = segue.destination as? ChatRoomObjectListViewController {
                controller.subscription = subscription
                controller.title = sender as?String
                if sender as? String == "メンバーリスト" {
                    controller.objectType = ObjectType.roomMember
                }else {
                    controller.objectType = ObjectType.file
                }
            }
        }else if segue.identifier == "User Profile" {
            if let controller = segue.destination as? UserProfileViewController {
                let user = sender as! User
                controller.user = user
            }
        }
    }
    
    func updateCurrentUserInformation() {
        guard let user = AuthManager.currentUser() else { return }
        profileView?.nameLabel.text = user.name ?? "氏名"
        profileView?.userIDLabel.text = user.username ?? "@no_username"
        profileView?.descriptionLabel.text = "所属部署\nチーム等"
        profileView?.avatarView.user = user
    }
    
    fileprivate func setupTextViewSettings() {
        textInputbar.autoHideRightButton = false // 送信ボタンを常に表示(trueだと動的に表示)
        textView.registerMarkdownFormattingSymbol("*", withTitle: "Bold")
        textView.registerMarkdownFormattingSymbol("_", withTitle: "Italic")
        textView.registerMarkdownFormattingSymbol("~", withTitle: "Strike")
        textView.registerMarkdownFormattingSymbol("`", withTitle: "Code")
        textView.registerMarkdownFormattingSymbol("```", withTitle: "Preformatted")
        textView.registerMarkdownFormattingSymbol(">", withTitle: "Quote")
        
        registerPrefixes(forAutoCompletion: ["@", "#"])
    }
    
    fileprivate func setupTitleView() {
        let view = ChatTitleView.instantiateFromNib()
        self.navigationItem.titleView = view
        chatTitleView = view
        
        let infoGesture = UITapGestureRecognizer(target: self, action: #selector(chatTitleViewDidPressed))
        chatTitleView?.labelTitle.addGestureRecognizer(infoGesture)
    }
    
    fileprivate func setupScrollToBottomButton() {
        buttonScrollToBottom.layer.cornerRadius = 25
        buttonScrollToBottom.layer.borderColor = UIColor.lightGray.cgColor
        buttonScrollToBottom.layer.borderWidth = 1
    }
    
    override class func collectionViewLayout(for decoder: NSCoder) -> UICollectionViewLayout {
        return UICollectionViewFlowLayout()
    }
    
    fileprivate func registerCells() {
        collectionView?.register(UINib(
            nibName: "ChatMessageCell",
            bundle: Bundle.main
        ), forCellWithReuseIdentifier: ChatMessageCell.identifier)
        
        collectionView?.register(UINib(
            nibName: "ChatMessageDaySeparator",
            bundle: Bundle.main
        ), forCellWithReuseIdentifier: ChatMessageDaySeparator.identifier)
        
        autoCompletionView.register(UINib(
            nibName: "AutocompleteCell",
            bundle: Bundle.main
        ), forCellReuseIdentifier: AutocompleteCell.identifier)
    }
    
    fileprivate func scrollToBottom(_ animated: Bool = false) {
        if self.needTargetJump { return }
        
        let totalItems = (collectionView?.numberOfItems(inSection: 0) ?? 0) - 1
        
        if totalItems > 0 {
            let indexPath = IndexPath(row: totalItems, section: 0)
            collectionView?.scrollToItem(at: indexPath, at: .bottom, animated: animated)
        }
        
        hideButtonScrollToBottom(animated: true)
    }
    
    fileprivate func hideButtonScrollToBottom(animated: Bool) {
        buttonScrollToBottomMarginConstraint?.constant = 50
        let action = {
            self.view.layoutIfNeeded()
        }
        if animated {
            UIView.animate(withDuration: 0.5, animations: action)
        } else {
            action()
        }
    }
    
    // MARK: Keyboard Function
    
    override func keyboardWillShow(_ notification: Notification) {
        self.keyboardWillBeShown(notification: notification)
    }
    
    override func keyboardWillHide(_ notification: Notification) {
        keyboardWillBeHidden(notification: notification)
    }
    
    func keyboardWillBeShown(notification: Notification) {
        if let userInfo = notification.userInfo {
            let frame = (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
            let animationDuration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue
            if (frame != nil && animationDuration != nil) {
                keyboardFrame = frame!
                // 絵文字ビューを小さくする
                emojiView?.frame.size.height = self.view.frame.height - (frame?.height)! - textInputbar.frame.height
            }
        }
    }
    
    func keyboardWillBeHidden(notification: Notification) {
        // 絵文字ビューを通常に戻す
        emojiView?.frame.size.height = self.view.frame.height - textInputbar.frame.height
    }
    
    // MARK: SlackTextViewController
    
    override func canPressRightButton() -> Bool {
        return SocketManager.isConnected()
    }
    
    override func didPressRightButton(_ sender: Any?) {
        sendMessage()
    }
    
    override func didPressLeftButton(_ sender: Any?) {
        buttonUploadDidPressed()
    }
    
    override func didPressReturnKey(_ keyCommand: UIKeyCommand?) {
        sendMessage()
    }
    
    override func textViewDidBeginEditing(_ textView: UITextView) {
        scrollToBottom()
    }
    
    override func textViewDidChange(_ textView: UITextView) {
    }
    
    // テキスト入力領域のサイズを調整
    override func textDidUpdate(_ animated: Bool) {
        super.textDidUpdate(animated) // trueだと絵文字ボタンも弾んで不自然になる
        
        let textOrigin = textInputbar.textView.frame.origin.x
        if textOrigin > 0 {
            if emojiOriginX != nil && buttonEmoji != nil {
                // 絵文字を下に固定
                buttonEmoji.frame = CGRect(x:emojiOriginX, y:self.textInputbar.rightButton.frame.origin.y, width: 20.0, height: 20.0)
                
            }else {
                // 初期表示の幅をここで指定
                initTextViewMargins()
            }
        }
    }
    
    func initTextViewMargins() {
        emojiOriginX = textInputbar.frame.size.width - textInputbar.rightButton.frame.width - 10
    }
    
    // MARK: Message
    
    fileprivate func sendMessage() {
        guard let message = textView.text else { return }
        
        if message.characters.count == 0 {
            return
        }
        
        rightButton.isEnabled = false
        
        // 編集の場合
        if messageForUpdate != nil {
            Realm.execute { realm in
                messageForUpdate!.text = message
            }
            
            MessageManager.editMessage(messageForUpdate!, completion: { _ in
                self.messageForUpdate = nil
                self.textView.text = ""
                self.rightButton.isEnabled = true
                
                // 再読み込み
                self.updateSubscriptionInfo()
                self.scrollToBottom()
            })
            
            return
        }
        
        SubscriptionManager.sendTextMessage(message, subscription: subscription) { [weak self] _ in
            self?.textView.text = ""
            self?.rightButton.isEnabled = true
            self?.scrollToBottom()
        }
    }
    
    func didTapEmojiButton() {
        emojiView?.isHidden = false
        
        // 開くたびに毎回スタンプ取得
        MessageManager.getListEmojiCustom({(list) in
            self.emojiView?.stampList = list
        })
    }
    
    // MARK: Subscription
    
    fileprivate func markAsRead() {
        SubscriptionManager.markAsRead(subscription) { _ in
            // 一括既読処理
            SubscriptionManager.markReadOnRoom(room: self.subscription.rid) {_ in
                
            }
        }
    }
    
    internal func updateSubscriptionInfo() {
        if let token = messagesToken {
            token.stop()
        }
        
        title = subscription?.name
        chatTitleView?.subscription = subscription
        textView.resignFirstResponder()
        
        collectionView?.performBatchUpdates({
            let indexPaths = self.dataController.clear()
            self.collectionView?.deleteItems(at: indexPaths)
        }, completion: { _ in
            CATransaction.commit()
            
            if self.closeSidebarAfterSubscriptionUpdate {
                MainChatViewController.closeSideMenuIfNeeded()
                self.closeSidebarAfterSubscriptionUpdate = false
            }
        })
        
        if self.subscription.isValid() {
            self.updateSubscriptionMessages()
        } else {
            self.subscription.fetchRoomIdentifier({ [weak self] response in
                self?.subscription = response
            })
        }
        
        if subscription.isJoined() {
            setTextInputbarHidden(false, animated: false)
            chatPreviewModeView?.removeFromSuperview()
        } else {
            setTextInputbarHidden(true, animated: false)
            showChatPreviewModeView()
        }
    }
    
    internal func updateSubscriptionMessages() {
        isRequestingHistory = true
        
        messages = subscription.fetchMessages()
        appendMessages(messages: Array(messages), updateScrollPosition: false, completion: {
            guard let messages = self.messages else { return }
            
            if messages.count == 0 {
                self.activityIndicator.startAnimating()
            } else {
                self.scrollToBottom()
            }
        })
        
        messagesToken = messages.addNotificationBlock { [weak self] _ in
            guard let isRequestingHistory = self?.isRequestingHistory, !isRequestingHistory else { return }
            guard let messages = self?.subscription.fetchMessages() else { return }
            
            self?.appendMessages(messages: Array(messages), updateScrollPosition: true, completion: nil)
        }
        
        MessageManager.getHistory(subscription, lastMessageDate: nil) { [weak self] _ in
            guard let messages = self?.subscription.fetchMessages() else { return }
            
            self?.appendMessages(messages: Array(messages), updateScrollPosition: false, completion: {
                self?.activityIndicator.stopAnimating()
                self?.scrollToBottom()
                self?.isRequestingHistory = false
            })
        }
        
        MessageManager.changes(subscription)
    }
    
    fileprivate func loadMoreMessagesFrom(date: Date?) {
        if isRequestingHistory {
            return
        }
        
        isRequestingHistory = true
        MessageManager.getHistory(subscription, lastMessageDate: date) { [weak self] newMessages in
            self?.appendMessages(messages: newMessages, updateScrollPosition: true, completion: {_ in
                if (self?.needTargetJump)! {
                    self?.jumpMessage(targetMessage: (self?.targetMessage)!)
                }
                self?.isRequestingHistory = false
                
                //                self?.collectionView?.reloadData()
            })
        }
    }
    
    fileprivate func appendMessages(messages: [Message], updateScrollPosition: Bool = false, completion: VoidCompletion?) {
        guard let collectionView = self.collectionView else { return }
        
        var contentHeight = collectionView.contentSize.height
        var offsetY = collectionView.contentOffset.y
        var bottomOffset = contentHeight - offsetY
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        UIView.performWithoutAnimation {
            collectionView.performBatchUpdates({
                // Insert data into collectionView without moving it
                contentHeight = collectionView.contentSize.height
                offsetY = collectionView.contentOffset.y
                bottomOffset = contentHeight - offsetY
                
                var objs: [ChatData] = []
                var newMessages: [Message] = []
                
                // Do not add duplicated messages
                for message in messages {
                    var insert = true
                    
                    // swiftlint:disable for_where
                    for obj in self.dataController.data {
                        if message.identifier == obj.message?.identifier {
                            insert = false
                        }
                    }
                    
                    if insert {
                        newMessages.append(message)
                    }
                }
                
                // Normalize data into ChatData object
                for message in newMessages {
                    guard let createdAt = message.createdAt else { continue }
                    guard var obj = ChatData(type: .message, timestamp: createdAt) else { continue }
                    obj.message = message
                    objs.append(obj)
                }
                
                // No new data? Don't update it then
                if objs.count == 0 {
                    return
                }
                
                let indexPaths = self.dataController.insert(objs)
                collectionView.insertItems(at: indexPaths)
            }, completion: { _ in
                let shouldScroll = self.isContentBiggerThanContainerHeight()
                if updateScrollPosition && shouldScroll {
                    let offset = CGPoint(x: 0, y: collectionView.contentSize.height - bottomOffset)
                    collectionView.contentOffset = offset
                }
                
                completion?()
                
                CATransaction.commit()
            })
        }
    }
    
    fileprivate func showChatPreviewModeView() {
        chatPreviewModeView?.removeFromSuperview()
        
        if let previewView = ChatPreviewModeView.instantiateFromNib() {
            previewView.delegate = self
            previewView.subscription = subscription
            previewView.frame = CGRect(x: 0, y: view.frame.height - previewView.frame.height, width: view.frame.width, height: previewView.frame.height)
            view.addSubview(previewView)
            chatPreviewModeView = previewView
        }
    }
    
    fileprivate func isContentBiggerThanContainerHeight() -> Bool {
        if let contentHeight = self.collectionView?.contentSize.height {
            if let collectionViewHeight = self.collectionView?.frame.height {
                if contentHeight < collectionViewHeight {
                    return false
                }
            }
        }
        
        return true
    }
    
    // MARK: IBAction
    
    func didSelectSubscriptionUserIcon(user: User) {
        // 左ドロワーのDMユーザーの画像がタップされた時
        performSegue(withIdentifier: "User Profile", sender: user)
    }
    
    func chatTitleViewDidPressed(_ sender: AnyObject) {
        if subscription.type != .directMessage {
            performSegue(withIdentifier: "Channel Info", sender: sender)
        }
    }
    
    func closeProfileView() {
        // 右ドロワーを非表示
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.profileView?.frame = CGRect(x: self.view.frame.size.width, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        })
        isShowProfile = false
    }
    
    @IBAction func buttonScrollToBottomPressed(_ sender: UIButton) {
        scrollToBottom(true)
    }
}

// MARK: UICollectionViewDataSource

extension ChatViewController {
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if let message = dataController.itemAt(indexPath)?.message {
                loadMoreMessagesFrom(date: message.createdAt)
            } else {
                let nextIndexPath = IndexPath(row: indexPath.row + 1, section: indexPath.section)
                
                if let message = dataController.itemAt(nextIndexPath)?.message {
                    loadMoreMessagesFrom(date: message.createdAt)
                }
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataController.data.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard dataController.data.count > indexPath.row else { return UICollectionViewCell() }
        guard let obj = dataController.itemAt(indexPath) else { return UICollectionViewCell() }
        
        if obj.type == .message {
            return cellForMessage(obj, at: indexPath)
        }
        
        if obj.type == .daySeparator {
            return cellForDaySeparator(obj, at: indexPath)
        }
        
        return UICollectionViewCell()
    }
    
    // MARK: Cells
    
    func cellForMessage(_ obj: ChatData, at indexPath: IndexPath) -> UICollectionViewCell {
        let message = obj.message
        
        if let cell = collectionView?.dequeueReusableCell(withReuseIdentifier: ChatMessageCell.identifier, for: indexPath) as? ChatMessageCell {
            cell.delegate = self
            cell.message = message
            return cell
        }else{
            return UICollectionViewCell()
        }
    }
    
    func cellForDaySeparator(_ obj: ChatData, at indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView?.dequeueReusableCell(
            withReuseIdentifier: ChatMessageDaySeparator.identifier,
            for: indexPath
            ) as? ChatMessageDaySeparator else {
                return UICollectionViewCell()
        }
        cell.labelTitle.text = obj.timestamp.formatted("MMM dd, YYYY")
        return cell
    }
    
}

// MARK: UICollectionViewDelegateFlowLayout

extension ChatViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let fullWidth = UIScreen.main.bounds.size.width
        
        if AuthManager.isAuthenticated() == nil {
            return CGSize(width: fullWidth, height: 40)
        }
        
        if let message = dataController.itemAt(indexPath)?.message {
            let height = ChatMessageCell.cellMediaHeightFor(message: message)
            return CGSize(width: fullWidth, height: height)
        }
        
        return CGSize(width: fullWidth, height: 40)
    }
}

// MARK: UIScrollViewDelegate

extension ChatViewController {
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        guard let view = buttonScrollToBottom.superview else { return }
        
        if buttonScrollToBottomMarginConstraint == nil {
            buttonScrollToBottomMarginConstraint = buttonScrollToBottom.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 50)
            buttonScrollToBottomMarginConstraint?.isActive = true
        }
        if targetContentOffset.pointee.y < scrollView.contentSize.height - scrollView.frame.height {
            buttonScrollToBottomMarginConstraint?.constant = -64
            UIView.animate(withDuration: 0.5) {
                view.layoutIfNeeded()
            }
        } else {
            hideButtonScrollToBottom(animated: true)
        }
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.needTargetJump = false
    }
}

// MARK: ChatPreviewModeViewProtocol

extension ChatViewController: ChatPreviewModeViewProtocol {
    
    func userDidJoinedSubscription() {
        guard let auth = AuthManager.isAuthenticated() else { return }
        guard let subscription = self.subscription else { return }
        
        Realm.execute { _ in
            subscription.auth = auth
        }
        
        self.subscription = subscription
    }
    
}

// MARK: ChatTitleViewDelegate

extension ChatViewController: ChatTitleViewDelegate {
    
    func didTapSearchButton() {
        // プロフィール表示中は何もしない
        if isShowProfile {
            return
        }
        
        // 検索窓を生成
        if searchHeaderView == nil {
            if let headerView = ChatHeaderSearchView.instantiateFromNib() {
                headerView.frame = CGRect(x: 0, y: -headerView.frame.height, width: view.frame.width, height: headerView.frame.height)
                searchHeaderView = headerView
                searchHeaderView.searchBar.delegate = self
                self.view.insertSubview(searchHeaderView, belowSubview: profileView!)
            }
        }
        
        // 表示・非表示切り替え
        var newFrame = searchHeaderView.frame
        if newFrame.origin.y == 0 {
            newFrame.origin.y = -newFrame.height
            // 非表示にする時にキーボードを下げる
            self.searchHeaderView.searchBar.resignFirstResponder()
        } else {
            newFrame.origin.y = 0.0
        }
        UIView.animate(withDuration: 0.3) {
            self.searchHeaderView.frame = newFrame
        }
    }
    
    func didTapProfileButton() {
        
        // キーボード閉じる
        textView.resignFirstResponder()
        if searchHeaderView != nil {
            self.searchHeaderView.searchBar.resignFirstResponder()
        }
        
        if isShowProfile {
            // 右ドロワーを非表示
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                self.profileView?.frame = CGRect(x: self.view.frame.size.width, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            })
            isShowProfile = false
        }
        else {
            // 右ドロワーを表示
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                self.profileView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            })
            isShowProfile = true
        }
    }
}

// MARK: ChatProfileViewDelegate
extension ChatViewController: ChatProfileViewDelegate {
    
    func didTapProfileListButton(tag: ProfileButtonTag) {
        var title: String!
        var segueIdentifier: String!
        
        switch tag {
        case ProfileButtonTag.memberList:
            title = "メンバーリスト"
            segueIdentifier = "Member List"
            break
        case ProfileButtonTag.mention:
            title = "メンション"
            segueIdentifier = "Message List"
            break
        case ProfileButtonTag.bookmark:
            title = "ブックマーク"
            segueIdentifier = "Message List"
            break
        case ProfileButtonTag.file:
            title = "ファイル"
            segueIdentifier = "Member List"
            break
        case ProfileButtonTag.pin:
            title = "ピンしたメッセージ"
            segueIdentifier = "Message List"
            break
        }
        
        self.performSegue(withIdentifier: segueIdentifier, sender: title)
    }
    
    // 設定ボタン押下時の処理
    func didTapProfileSettingButton() {
        let storyboard = UIStoryboard(name: "Settings", bundle: Bundle.main)
        
        if let controller = storyboard.instantiateInitialViewController() {
            controller.modalPresentationStyle = .formSheet
            
            if controller.isKind(of: UINavigationController.self) {
                let con = controller as! UINavigationController
                let root = con.viewControllers.first
                if (root?.isMember(of: SettingsViewController.self))! {
                    let vc = root as! SettingsViewController
                    vc.subscription = self.subscription
                }
            }
            
            self.navigationController?.present(controller, animated: true, completion: nil)
        }
    }
    
    // ステータスボタン押下時の処理
    func didTapProfileStatusButton() {
        guard let viewUserMenu = SubscriptionUserStatusView.instantiateFromNib() else { return }
        
        let user = AuthManager.currentUser()!
        viewUserMenu.userNameLabel.text = user.username
        
        switch user.status {
        case .online:
            viewUserMenu.currentStatus.backgroundColor = .RCOnline()
            break
        case .busy:
            viewUserMenu.currentStatus.backgroundColor = .RCBusy()
            break
        case .away:
            viewUserMenu.currentStatus.backgroundColor = .RCAway()
            break
        case .offline:
            viewUserMenu.currentStatus.backgroundColor = .RCInvisible()
            break
        }
        
        var newFrame = view.frame
        newFrame.origin.x = newFrame.width
        newFrame.origin.y = 0
        viewUserMenu.frame = newFrame
        viewUserMenu.delegate = self
        viewUserMenu.parentController = self
        
        view.addSubview(viewUserMenu)
        self.viewUserMenu = viewUserMenu
        
        newFrame.origin.x = 45
        UIView.animate(withDuration: 0.3) {
            viewUserMenu.frame = newFrame
        }
    }
    
    func dismissUserMenu() {
        guard let viewUserMenu = viewUserMenu else { return }
        
        var newFrame = viewUserMenu.frame
        newFrame.origin.x = 50 + newFrame.width
        
        UIView.animate(withDuration: 0.3, animations: {
            viewUserMenu.frame = newFrame
        }) { (_) in
            viewUserMenu.removeFromSuperview()
        }
    }
}

// MARK: SubscriptionUserStatusViewProtocol
extension ChatViewController: SubscriptionUserStatusViewProtocol {
    func userDidPressedOption() {
        dismissUserMenu()
    }
    
    func userDidPressedClose() {
        self.dismissUserMenu()
    }
}

// MARK: ChatEmojiViewDelegate
extension ChatViewController: ChatEmojiViewDelegate {
    func didSelectEmojiButton(emojiString: String) {
        // テキストに絵文字を追加
        self.textView.text.append(Emojione.transform(string: emojiString))
        self.emojiView?.isHidden = true
    }
    
    func didSelectCustomStampButton(stamp:CustomStamp) {
        self.emojiView?.isHidden = true
        
        // スタンプ送信
        rightButton.isEnabled = false
        
        let msg = stamp.name + "." + stamp.ext
        SubscriptionManager.sendStampMessage(msg, subscription: subscription) { [weak self] _ in
            self?.rightButton.isEnabled = true
        }
    }
}

// MARK: UISearchBarDelegate
extension ChatViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        var searchedMessages = List<Message>()
        if (searchBar.text != nil) {
            searchedMessages = SubscriptionManager.messageSearch(room: subscription.rid, searchText: searchBar.text!, subscription: subscription, completion: { (results) in
                self.performSegue(withIdentifier: "Searched List", sender: searchedMessages)
            })
        }
    }
}

// MARK: ChatMessageListViewControllerDelegate
extension ChatViewController: ChatMessageListViewControllerDelegate {
    // 該当メッセージへジャンプ
    func jumpMessage(targetMessage:Message) {
        self.needTargetJump = true
        if self.targetMessage != targetMessage {
            self.targetMessage = targetMessage
        }
        
        
        for i in 0..<self.dataController.data.count {
            let indexPath = IndexPath(row: i, section: 0)
            let obj = dataController.itemAt(indexPath)
            if obj?.type == .message {
                if targetMessage.identifier == obj?.message?.identifier {
                    let indexPath = IndexPath(row: i, section: 0)
                    self.collectionView?.scrollToItem(at: indexPath, at: .top, animated: false)
                    return
                }
            }
        }
        
        // 該当メッセージがなかった時はさらに読み込む
        if let message = dataController.itemAt(IndexPath(row: 0, section: 0))?.message {
            loadMoreMessagesFrom(date: message.createdAt)
        } else {
            let nextIndexPath = IndexPath(row: 1, section: 0)
            
            if let message = dataController.itemAt(nextIndexPath)?.message {
                loadMoreMessagesFrom(date: message.createdAt)
            }
        }
    }
}

// MARK: ChannelInfoViewControllerDelegate
extension ChatViewController: ChannelInfoViewControllerDelegate {
    // グループ名の変更を反映
    func didChangeChannelName(newName:String) {
        self.title = newName
        self.chatTitleView?.labelTitle.text = newName
    }
}


// MARK: ChatMessageDetailControllerDelegate
extension ChatViewController: ChatMessageDetailControllerDelegate {
    // 返信
    func didTapReplyButtonWithUserName(name:String) {
        self.textView.text = "@" + "\(name)"
    }
    
    // 引用返信
    func didTapReplyButtonWithMessage(replyMessage :Message) {
        self.textView.text = "@" + "\(replyMessage.user!.username!)" + "\n>" + "\(replyMessage.text)"
    }
    
    func didDeleteMessage() {
        // 一度空にする
        _ = self.dataController.clear()
        self.collectionView?.reloadData()
        
        // メッセージの取得し直し
        if isRequestingHistory {
            return
        }
        
        isRequestingHistory = true
        MessageManager.getHistory(subscription, lastMessageDate: nil) { [weak self] newMessages in
            self?.appendMessages(messages: newMessages, updateScrollPosition: true, completion: {_ in
                self?.collectionView?.reloadData()
                self?.scrollToBottom()
            })
            self?.isRequestingHistory = false
        }
    }
}
