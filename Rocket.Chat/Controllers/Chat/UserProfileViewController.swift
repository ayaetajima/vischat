//
//  UserProfileViewController.swift
//  Rocket.Chat
//
//  Created by Rafael Kellermann Streit on 09/12/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import Foundation

final class UserProfileViewController: UIViewController {
    
    @IBOutlet var bgImageView: UIImageView!
    @IBOutlet var labelUserName: UILabel!
    @IBOutlet var labelDispName: UILabel!
    @IBOutlet var labelDeptName: UILabel!
    @IBOutlet var labelEmail: UILabel!
    @IBOutlet var labelLastLogin: UILabel!
    @IBOutlet var messageButton: UIButton!
    @IBOutlet weak var avatarViewContainer: UIView! {
        didSet {
            if let avatarView = AvatarView.instantiateFromNib() {
                avatarView.frame = avatarViewContainer.bounds
                avatarViewContainer.addSubview(avatarView)
                avatarViewContainer.layer.cornerRadius = avatarView.frame.size.width/2
                avatarViewContainer.layer.masksToBounds = true

                self.avatarView = avatarView
            }
        }
    }
    
    weak var avatarView: AvatarView!
    var user = User()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
                
        self.navigationController?.isNavigationBarHidden = true
        updateUserInfomation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func updateUserInfomation(){
        self.labelDispName.text = user.username
        self.avatarView.user = user

        UserManager.getUserInfoOfName(username: user.username!, completion: {(response)in
            self.user = response
            self.labelUserName.text = response.name
            self.labelDeptName.text = response.deptName
            self.labelEmail.text = response.emails[0].email
            
            if let lastLogin = response.lastLogin {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy年MM月dd日 HH:mm"
                self.labelLastLogin.text = formatter.string(from: lastLogin)
            }
        })
    }

    @IBAction func didTapMessageButton(sender: UIButton){
        // その人にメッセージを送る
        guard let auth = AuthManager.isAuthenticated() else { return }
        var subscriptions = auth.subscriptions.filter("name == %@", user.username!)
        if subscriptions.count == 0 {
            // 新規でDMグループを作る
            SubscriptionManager.createDirectMessage(user.username!, completion: {_ in
                // 再検索
                subscriptions = auth.subscriptions.filter("name == %@", self.user.username!)
                let subscription = subscriptions[0]
                self.popViewControllerWithSubscription(sub: subscription)
            })
        }
        else {
            let subscription = subscriptions[0]
            self.popViewControllerWithSubscription(sub: subscription)
        }
    }

    private func popViewControllerWithSubscription(sub: Subscription){
        let controller = ChatViewController.sharedInstance()
        controller?.subscription = sub
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapCloseButton(sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapCallButton(sender: UIButton){
        // 電話を呼び出す
    }
    
    @IBAction func didTapDetailMenuButton(sender: UIButton){
        // アクションシートを表示
        let alertSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let action1 = UIAlertAction(title: "添付ファイル", style: UIAlertActionStyle.default, handler: {
            (action: UIAlertAction!) -> Void in
            // 添付ファイル一覧画面へ遷移
            self.performSegue(withIdentifier: "View Files", sender: self)
        })
        let action2 = UIAlertAction(title: "cancel", style: UIAlertActionStyle.cancel, handler: {
            (action: UIAlertAction!) in
        })
        
        alertSheet.addAction(action1)
        alertSheet.addAction(action2)
        
        self.present(alertSheet, animated: true, completion: nil)
    }
}
