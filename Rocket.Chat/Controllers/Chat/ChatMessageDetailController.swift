//
//  ChatMessageDetailController.swift
//  Rocket.Chat
//
//  Created by Rafael Kellermann Streit on 09/12/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

protocol ChatMessageDetailControllerDelegate {
    // 返信
    func didTapReplyButtonWithUserName(name:String)
    // 引用返信
    func didTapReplyButtonWithMessage(replyMessage:Message)
    func didDeleteMessage()
}

final class ChatMessageDetailController: UIViewController {
    
    @IBOutlet weak var readListLabel: UILabel!
    
    @IBOutlet weak var avatarViewContainer: AvatarView! {
        didSet {
            if let avatarView = AvatarView.instantiateFromNib() {
                avatarView.frame = avatarViewContainer.bounds
                avatarViewContainer.layer.cornerRadius = avatarView.frame.size.width/2
                avatarViewContainer.layer.masksToBounds = true
                
                avatarViewContainer.addSubview(avatarView)
                self.avatarView = avatarView
            }
        }
    }

    weak var avatarView: AvatarView!
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var labelText: UITextView!
    @IBOutlet weak var stampImageView: UIImageView!

    @IBOutlet weak var bookmarkButton: UIButton!
    
    var delegate: ChatMessageDetailControllerDelegate?

    var subscription: Subscription!
    var message: Message!
    
    var starredId:String = ""
    
    func updateMessage(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy年MM月dd日 HH:mm"
        
        if let createdAt = message.createdAt {
            labelDate.text = formatter.string(from: createdAt)
        }
        
        avatarView.user = message.user
        labelUsername.text = message.user?.username
        readListLabel.text = "\(message.getReaderComponents().count)" + "人が既読しました"
        
        // スタンプの場合
        if message.internalType == "stamp" {
            insertStamp(imageView: stampImageView)
            stampImageView.isHidden = false
            return
        }
        else {
            stampImageView.isHidden = true
        }

        let text = NSMutableAttributedString(string: message.textNormalized())
        
        if self.message.isSystemMessage() {
            text.setFont(MessageTextFontAttributes.italicFont)
            text.setFontColor(MessageTextFontAttributes.systemFontColor)
        } else {
            text.setFont(MessageTextFontAttributes.defaultFont)
            text.setFontColor(MessageTextFontAttributes.defaultFontColor)
        }
        
        //print(message.starreds.description)

        labelText.sizeToFit()
        labelText.attributedText = text.transformMarkdown()
        
        if (message.starred != "null" && message.starred != ""){
            let splitComma = message.starred.components(separatedBy: ",")
            
            if(splitComma.count != 0){
                for i in (0 ..< splitComma.count) {
                    
                    let split = splitComma[i].components(separatedBy: "\"")
                    if (split[3] == subscription.auth?.userId){
                        starredId = split[3]
                    }
                }
            }else{
                let split = message.starred.components(separatedBy: "\"")
                starredId = split[3]
            }

        }else{
            starredId = ""
        }

        let img = starredId == subscription.auth?.userId ? "Star-Filled" : "Star"
        bookmarkButton.setImage(UIImage(named: img), for: UIControlState.normal)
    }
    
    func insertStamp(imageView: UIImageView) {
        let path = NSString(string: message.text)
        let ext = path.pathExtension
        let name = path.replacingOccurrences(of: "." + ext, with: "")
        
        let stamp = CustomStamp()
        stamp.name = name
        stamp.ext = ext
        let imageURL = stamp.fullImageURL()
        
        imageView.sd_setImage(with: imageURL, completed: {_ in
            imageView.frame.size.height = 150
            imageView.contentMode = .scaleAspectFit
        })
    }

    func updateButtonStarImage(_ force: Bool = false, value: Bool = false) {
        guard let bookmarkButton = self.bookmarkButton else { return }
        let starred = force ? value : starredId == subscription.auth?.userId ? true:false
        var image: UIImage?
        
        if starred {
            image = UIImage(named: "Star-Filled")?.imageWithTint(UIColor.RCFavoriteMark())
        } else {
            image = UIImage(named: "Star")?.imageWithTint(UIColor.RCGray())
        }
        
        bookmarkButton.setImage(image, for: UIControlState.normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapReadListLabel))
        gesture.delegate = self
        readListLabel.addGestureRecognizer(gesture)

        updateMessage()
        updateButtonStarImage()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Read List" {
            if let controller = segue.destination as? ChatRoomObjectListViewController{
                controller.title = "既読リスト"
                controller.objectType = .reader
                controller.readerNameList = message.getReaderComponents()
            }
        }
    }
    
    
    func didTapReadListLabel() {
        // 既読リストに遷移
        self.performSegue(withIdentifier: "Read List", sender: self)
    }
    
    @IBAction func didTapBookmarkButton(sender: UIButton) {
        // スターをつける・解除
        if starredId == subscription.auth?.userId {
            MessageManager.unstar(self.message, completion: { (_) in
                self.starredId = ""
                Realm.execute { (_) in
                    self.message.starred = ""
                }
                self.updateButtonStarImage()
            })
        } else {
            MessageManager.star(self.message, completion: { (_) in
                
                Realm.execute { (_) in
                    self.message.starred = "{\n  \"_id\" : \"" + (self.subscription.auth?.userId)! + "\"\n}"
                }
                self.starredId = (self.subscription.auth?.userId)!
                
                self.updateButtonStarImage()
            })
        }
}
    
    @IBAction func didTapReplyButton(sender: UIButton) {
        // 返信する
        delegate?.didTapReplyButtonWithUserName(name: (message.user?.username)!)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapDetailMenuButton(ender: UIButton) {
        // 詳細メニュー
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // レポート
        alert.addAction(UIAlertAction(title: localized("chat.message.actions.report"), style: .destructive, handler: { (_) in
            // 選択肢を表示
            let alert = UIAlertController(
                title: "レポート",
                message: "不適切発言の種類を選択",
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "パワハラとして報告", style: .default, handler: { (_)in
                self.reportMessage(genre: "PowerHarassment")
            }))
            alert.addAction(UIAlertAction(title: "セクハラとして報告", style: .default, handler: { (_)in
                self.reportMessage(genre: "SexualHarassment")
            }))
            alert.addAction(UIAlertAction(title: localized("global.cancel"), style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            }))
        
        // 本文コピー
        alert.addAction(UIAlertAction(title: "本文コピー", style: .default, handler: { (_) in
            let pasteboard = UIPasteboard.general
            pasteboard.string = self.message.textNormalized()
            // Pasteboardに画像の保存
//            pasteboard.image = UIImage(name: "test.png")
        }))

        // 引用返信（他人のメッセージのみ）
        if subscription.auth?.userId != message.user?.identifier {
            alert.addAction(UIAlertAction(title: "引用して返信", style: .default, handler: { (_) in
                self.delegate?.didTapReplyButtonWithMessage(replyMessage: self.message)
                self.navigationController?.popViewController(animated: true)
            }))
        }
        // ピン留め
        let str = message.pinned ? localized("chat.message.actions.unpin") : localized("chat.message.actions.pin")
        alert.addAction(UIAlertAction(title: str, style: .default, handler: { (_) in
            if self.message.pinned {
                MessageManager.unpin(self.message, completion: { (_) in
                    // Do nothing
                })
            } else {
                MessageManager.pin(self.message, completion: { (_) in
                    // Do nothing
                })
            }
        }))
        
        // 削除（自分のメッセージのみ、もしくはadmin権限の時）
        let user = AuthManager.currentUser()!
        let roles = user.roles
        if (subscription.auth?.userId == message.user?.identifier) || (roles == "admin"){
            alert.addAction(UIAlertAction(title: "削除", style: .destructive, handler: { (_) in
                // 確認アラート？
                let alert = UIAlertController(
                    title: "",
                    message: "このメッセージを削除してよろしいですか？",
                    preferredStyle: .alert
                )
                
                alert.addAction(UIAlertAction(title: localized("global.ok"), style: .default, handler: { (_) in
                    MessageManager.delete(self.message, completion: {(_) in
                        self.delegate?.didDeleteMessage()
                        self.navigationController?.popViewController(animated: true)
                    })
                }))
                
                alert.addAction(UIAlertAction(title: localized("global.cancel"), style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }))
        }

        // キャンセル
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
        }))

        present(alert, animated: true, completion: nil)
    }

    func reportMessage(genre:String) {
        // レポート
        MessageManager.report(self.message, genre:genre) { (_) in
            let alert = UIAlertController(
                title: localized("chat.message.report.success.title"),
                message: localized("chat.message.report.success.message"),
                preferredStyle: .alert
            )
            
            alert.addAction(UIAlertAction(title: localized("global.ok"), style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}

extension ChatMessageDetailController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
}
