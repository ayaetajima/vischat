//
//  ChatMessageListCell.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 10/10/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import UIKit
import RealmSwift

protocol ChatMessageListViewControllerDelegate {
    func jumpMessage(targetMessage:Message)
}

enum SegueType {
    case push
    case modal
}

final class ChatMessageListViewController: UITableViewController {

    var delegate: ChatMessageListViewControllerDelegate?
    var dataController = ChatDataController()
//    var isRequestingHistory = false
    var subscription: Subscription!
    var messageList = List<Message>()
    
    @IBAction func didTapCloseBarButton(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // API呼び出し
        print(self.title)
        
        if self.title == "メンション" {
            messageList = MessageManager.getMentionList(subscription, completion: {_ in
                self.tableView.reloadData()
            })

        }else if self.title == "ピンしたメッセージ" {
            messageList = MessageManager.getPinnedList(subscription, completion: {_ in
                self.tableView.reloadData()
            })

        }else if self.title == "ブックマーク" {
            messageList = MessageManager.getStaredList(subscription, completion: {_ in
                self.tableView.reloadData()
            })            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Message Detail" {
            if let controller = segue.destination as? ChatMessageDetailController {
                controller.subscription = subscription
                controller.message = sender as! Message
            }
        }
    }
    
    // MARK: UITableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageList.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if let message :Message = messageList[indexPath.row] {
            let height = ChatMessageListCell.cellMediaHeightFor(message: message)
            return height
        }
        
        return ChatMessageListCell.minimumHeight
    }
        
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: ChatMessageListCell.identifier) as? ChatMessageListCell {
            cell.message = messageList[indexPath.row]
            return cell
        }
        else {
           let cell = ChatMessageListCell.instantiateFromNib()
            cell?.delegate = self
            cell?.message = messageList[indexPath.row]

            return cell!
        }
    }
    
    // MARK: UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if self.title == "検索結果" {
            // 検索の時はホームへ戻って該当メッセージへジャンプ
            self.delegate?.jumpMessage(targetMessage: messageList[indexPath.row])
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        // メッセージ詳細へ遷移
        self.performSegue(withIdentifier: "Message Detail", sender: messageList[indexPath.row])
    }
    
}

extension ChatMessageListViewController:ChatMessageListCellProtocol
{
    func openURL(url: URL){
        
    }
    func handleLongPressMessageCell(_ message: Message, view: UIView, recognizer: UIGestureRecognizer){
        
    }

    func openURLFromCell(url: MessageURL)
    {
        
    }
    
    func openVideoFromCell(attachment: Attachment)
    {
        
    }
    
    func openImageFromCell(attachment: Attachment, thumbnail: UIImageView)
    {
        
    }
    func viewDidCollpaseChange(view: UIView)
    {
        
    }
}

