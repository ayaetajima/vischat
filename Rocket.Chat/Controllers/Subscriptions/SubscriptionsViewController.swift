//
//  SubscriptionsViewController.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 7/21/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import RealmSwift

// swiftlint:disable file_length
final class SubscriptionsViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityViewSearching: UIActivityIndicatorView!
    @IBOutlet weak var searchView: SubscriptionSerachView!    
    @IBOutlet weak var logoImageView: UIImageView! {
        didSet {
            loadLogoImage()
        }
    }
    @IBOutlet weak var tenantNameLabel: UILabel! {
        didSet {
            let ud = UserDefaults.standard
            let tenantName = ud.string(forKey: CookieManager.userDefaultKey_LoginTenantName)
            tenantNameLabel.text = tenantName
        }
    }
    
    var assigned = false
    var isSearchingLocally = false
    var isSearchingRemotely = false
    var searchResult: [Subscription]?
    var subscriptions: Results<Subscription>?
    var subscriptionsToken: NotificationToken?
    var usersToken: NotificationToken?

    var groupInfomation: [[String: String]]?
    var groupSubscriptions: [[Subscription]]?
    
    weak var viewDMSuggest: SubscriptionDMSuggestView?

    weak var viewCreateChannel: SubscriptionCreateChannelView?

    override func awakeFromNib() {
        super.awakeFromNib()
        subscribeModelChanges()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadLogoImage()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterKeyboardNotifications()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
        registerKeyboardHandlers(tableView)
    }
    
    func loadLogoImage() {
        if logoImageView.image == nil {
            guard let auth = AuthManager.isAuthenticated() else { return }
            guard let baseURL = auth.baseURL() else { return }
            let str = "\(baseURL)/avatar/"
            let imageURL = URL(string: str)
            
            logoImageView?.sd_setImage(with: imageURL, completed: { _ in
            })
        }
    }
}

extension SubscriptionsViewController {

    func searchBy(_ text: String = "") {

        // 検索はグループ名のみ
        if text.characters.count > 0 {
            // 自分が未所属のグループ一覧を取得
            searchOnSpotlight(text)
            return
        }

        // 検索文字列が空の時は自分が所属しているグループ、DMすべて
        guard let auth = AuthManager.isAuthenticated() else { return }
        subscriptions = auth.subscriptions.filter("name CONTAINS %@", text)

        if text.characters.count == 0 {

            isSearchingLocally = false
            isSearchingRemotely = false
            searchResult = []

            groupSubscription()
            tableView.reloadData()
            tableView.tableFooterView = nil

            activityViewSearching.stopAnimating()

            return
        }

        isSearchingLocally = true
        isSearchingRemotely = false

        groupSubscription()
        tableView.reloadData()

        if let footerView = SubscriptionSearchMoreView.instantiateFromNib() {
            footerView.delegate = self
            tableView.tableFooterView = footerView
        }
    }

    func searchOnSpotlight(_ text: String = "") {
        tableView.tableFooterView = nil
        activityViewSearching.startAnimating()

        SubscriptionManager.spotlight(text, rooms: true, users: false, completion: { [weak self] result in
            let currentText = self?.searchView.textFieldSearch.text ?? ""

            if currentText.characters.count == 0 {
                return
            }
            
            self?.activityViewSearching.stopAnimating()
            self?.isSearchingRemotely = true
            self?.searchResult = result
            self?.groupSubscription()
            self?.tableView.reloadData()
        })
    }

    func handleModelUpdates<T>(_: RealmCollectionChange<RealmSwift.Results<T>>?) {
        guard !isSearchingLocally && !isSearchingRemotely else { return }
        guard let auth = AuthManager.isAuthenticated() else { return }
        subscriptions = auth.subscriptions.sorted(byKeyPath: "lastSeen", ascending: false)
        groupSubscription()
        tableView?.reloadData()
    }

    func subscribeModelChanges() {
        guard !assigned else { return }
        guard let auth = AuthManager.isAuthenticated() else { return }

        assigned = true

        subscriptions = auth.subscriptions.sorted(byKeyPath: "lastSeen", ascending: false)
        subscriptionsToken = subscriptions?.addNotificationBlock(handleModelUpdates)
        usersToken = try? Realm().addNotificationBlock { [weak self] _, _ in
            self?.handleModelUpdates(nil)
        }

        groupSubscription()
    }

    // swiftlint:disable function_body_length cyclomatic_complexity
    func groupSubscription() {
//        var unreadGroup: [Subscription] = []
//        var favoriteGroup: [Subscription] = []
        var channelGroup: [Subscription] = []
        var directMessageGroup: [Subscription] = []
        var searchResultsGroup: [Subscription] = []

        guard let subscriptions = subscriptions else { return }
        let orderSubscriptions = isSearchingRemotely ? searchResult : Array(subscriptions.sorted(byKeyPath: "name", ascending: true))

        for subscription in orderSubscriptions ?? [] {
            if isSearchingRemotely {
                searchResultsGroup.append(subscription)
            }

            if !isSearchingLocally && !subscription.open {
                continue
            }

//            if subscription.alert {
//                unreadGroup.append(subscription)
//                continue
//            }

//            if subscription.favorite {
//                favoriteGroup.append(subscription)
//                continue
//            }

            switch subscription.type {
            case .channel, .group:
                channelGroup.append(subscription)
            case .directMessage:
                directMessageGroup.append(subscription)
            }
        }

        groupInfomation = [[String: String]]()
        groupSubscriptions = [[Subscription]]()

        if searchResultsGroup.count > 0 {
            groupInfomation?.append([
                "name": localized("subscriptions.search_results")
            ])

            searchResultsGroup = searchResultsGroup.sorted {
                return $0.name < $1.name
            }

            groupSubscriptions?.append(searchResultsGroup)
        } else {
            if channelGroup.count > 0 {
                groupInfomation?.append([
                    "name": localized("subscriptions.channels")
                ])

                groupSubscriptions?.append(channelGroup)
            }

            if directMessageGroup.count > 0 {
                groupInfomation?.append([
                    "name": localized("subscriptions.direct_messages")
                ])

                groupSubscriptions?.append(directMessageGroup)
            }
        }
    }
}

extension SubscriptionsViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return groupInfomation?.count ?? 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupSubscriptions?[section].count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SubscriptionCell.identifier) as? SubscriptionCell else {
            return UITableViewCell()
        }

        let subscription = groupSubscriptions?[indexPath.section][indexPath.row]
        cell.subscription = subscription        
        cell.delegate = self
        
        return cell
    }
}

extension SubscriptionsViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let group = groupInfomation?[section] else { return nil }
        guard let view = SubscriptionSectionView.instantiateFromNib() else {
            return nil
        }

        view.delegate = self
        view.tag = section
//        view.setIconName(group["icon"])
        let str = group["name"]
        view.setTitle(str)
        
        // addボタンの表示
        if ((str?.contains(localized("subscriptions.channels")))! ||
            (str?.contains(localized("subscriptions.direct_messages")))!) {
            view.buttonAdd.isHidden = false
        }else {
            view.buttonAdd.isHidden = true
        }

        return view
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchView.textFieldSearch.resignFirstResponder()
        
        let subscription = self.groupSubscriptions?[indexPath.section][indexPath.row]
        let controller = ChatViewController.sharedInstance()
        controller?.closeSidebarAfterSubscriptionUpdate = true
        controller?.subscription = subscription
    }
}

extension SubscriptionsViewController: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)

        if string == "\n" {
            if currentText.characters.count > 0 {
                searchOnSpotlight(currentText)
            }

            return false
        }

        searchBy(prospectiveText)
        return true
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        searchBy()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
}

extension SubscriptionsViewController: SubscriptionSearchMoreViewDelegate {

    func buttonLoadMoreDidPressed() {
        searchOnSpotlight(searchView.textFieldSearch.text ?? "")
    }
}

extension SubscriptionsViewController: SubscriptionCreateChannelViewProtocol {
    
    // グループ作成ボタンがタップされた通知
    func createChannelButtonPressed(name:String, isPrivate:Bool, isReadOnly:Bool) {
        // グループ追加
        guard let user = AuthManager.currentUser() else { return }
        if (viewCreateChannel?.switchPrivate.isOn)! {
            // プライベートグループ
            SubscriptionManager.createPrivateGroup(name, isReadOnly: isReadOnly, userName: user.username!, completion: {_ in
                // 作成画面を閉じる
                self.closeCreateChannelView()
            })
        }else {
            // パブリック
            SubscriptionManager.createNewChannel(name, isReadOnly: isReadOnly, userName: user.username!, completion: {_ in
                // 作成画面を閉じる
                self.closeCreateChannelView()
            })
        }
        
    }
    
    // 作成画面を閉じる
    func closeCreateChannelView() {
        guard let viewCreateChannel = viewCreateChannel else { return }
        
        var newFrame = viewCreateChannel.frame
        newFrame.origin.x = -newFrame.width
        
        UIView.animate(withDuration: 0.15, animations: {
            viewCreateChannel.frame = newFrame
        }) { (_) in
            viewCreateChannel.removeFromSuperview()
        }
    }
}

extension SubscriptionsViewController: SubscriptionSectionViewProtocol {
    
    // 追加ボタンがタップされた時
    func sectionAddButtonDidPressed(sender: SubscriptionSectionView) {
        guard let group = groupInfomation?[sender.tag] else { return }
        let str = group["name"]
        
        // addボタンの表示
        if (str?.contains(localized("subscriptions.channels")))!{
            // グループ追加画面を表示
            guard let viewCreateChannel = SubscriptionCreateChannelView.instantiateFromNib() else { return }
            
            var newFrame = view.frame
            newFrame.origin.x = -newFrame.width
            viewCreateChannel.frame = newFrame
            viewCreateChannel.delegate = self
            viewCreateChannel.parentController = self
            
            view.addSubview(viewCreateChannel)
            self.viewCreateChannel = viewCreateChannel
            
            newFrame.origin.x = 0
            UIView.animate(withDuration: 0.15) {
                viewCreateChannel.frame = newFrame
            }
        }
        else if (str?.contains(localized("subscriptions.direct_messages")))!{
            // DMサジェスト画面を表示
            guard let suggestView = SubscriptionDMSuggestView.instantiateFromNib() else {
                return }
            
            suggestView.delegate = self
            suggestView.searchType = .directMessage
            suggestView.frame.size.height = self.view.frame.height
            UIApplication.shared.keyWindow?.addSubview(suggestView)
            self.viewDMSuggest = suggestView
        }
    }
}

extension SubscriptionsViewController: SubscriptionDMSuggestViewProtocol
{
    func didSelect(subscription: Subscription) {
        self.viewDMSuggest?.removeFromSuperview()

        // DM作成画面
        let controller = ChatViewController.sharedInstance()
        controller?.closeSidebarAfterSubscriptionUpdate = true
        controller?.subscription = subscription
    }
    
    func closeSuggestView() {
        self.viewDMSuggest?.removeFromSuperview()
    }
}

extension SubscriptionsViewController: SubscriptionCellProtocol
{
    func didTapIconImage(user: User, recognizer: UITapGestureRecognizer) {
        // プロフィール画面に遷移
        MainChatViewController.closeSideMenuIfNeeded()
        ChatViewController.sharedInstance()?.didSelectSubscriptionUserIcon(user: user)
    }
}
