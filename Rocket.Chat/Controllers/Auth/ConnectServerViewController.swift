//
//  ConnectServerViewController.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 7/6/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import UIKit
import SwiftyJSON
import semver

final class ConnectServerViewController: BaseViewController {

    // Login設定情報(テスト環境)
//    internal var loginURL = URL(string: "https://chat.tabi.today/")
//    internal var wssSocketURL = URL(string: "wss://chat.tabi.today/websocket/")
    //*** Login設定情報(本番環境)
    internal var loginURL = URL(string: "https://et-chat.bizsky.jp")
    internal var wssSocketURL = URL(string: "wss://et-chat.bizsky.jp/websocket/")
    
    internal var serverURL: URL!

    var serverPublicSettings: AuthSettings?

    @IBOutlet weak var viewFields: UIView! {
        didSet {
            viewFields.layer.cornerRadius = 4
            viewFields.layer.borderColor = UIColor.RCLightGray().cgColor
            viewFields.layer.borderWidth = 0.5
        }
    }
    
    @IBOutlet weak var tutorialScrollView: UIScrollView!
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let nav = navigationController as? BaseNavigationController {
            nav.setTransparentTheme()
        }
        
        // チュートリアル画像セット
        tutorialScrollView.contentSize.width = self.view.frame.width * 5
        for i in 0..<5 {
            let imageView = UIImageView(image: UIImage(named: "tutorial_\(i)"))
            imageView.contentMode = UIViewContentMode.scaleAspectFit
            imageView.frame = CGRect.init(x: self.view.frame.width * CGFloat(i),
                                          y: 0,
                                          width: self.view.frame.width,
                                          height: self.view.frame.height)
            imageView.backgroundColor = UIColor.init(colorLiteralRed: 52/255, green: 152/255, blue: 219/255, alpha: 1.0)
            tutorialScrollView.addSubview(imageView)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    // MARK: IBAction

    @IBAction func didTapLoginButton() {
        guard let socketURL = wssSocketURL?.socketURL() else { return alertInvalidURL() }
        
        serverURL = socketURL

        if UIApplication.shared.canOpenURL(loginURL!){
            UIApplication.shared.openURL(loginURL!)
        }
    }
    
    func alertInvalidURL() {
        let alert = UIAlertController(
            title: localized("alert.connection.invalid_url.title"),
            message: localized("alert.connection.invalid_url.message"),
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(title: localized("global.ok"), style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    /*
    func validate(url: URL, completion: @escaping RequestCompletion) {
        let request = URLRequest(url: url)
        let session = URLSession.shared
        
        let task = session.dataTask(with: request, completionHandler: { (data, _, _) in
            if let data = data {
                let json = JSON(data: data)
                Log.debug(json.rawString())
                
                guard let version = json["version"].string else {
                    return completion(nil, true)
                }
                
                if let minVersion = Bundle.main.object(forInfoDictionaryKey: "RC_MIN_SERVER_VERSION") as? String {
                    if Semver.lt(version, minVersion) {
                        let alert = UIAlertController(
                            title: localized("alert.connection.invalid_version.title"),
                            message: String(format: localized("alert.connection.invalid_version.message"), version, minVersion),
                            preferredStyle: .alert
                        )
                        
                        alert.addAction(UIAlertAction(title: localized("global.ok"), style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
                completion(json, false)
            } else {
                completion(nil, true)
            }
        })
        
        task.resume()
    }*/

}
