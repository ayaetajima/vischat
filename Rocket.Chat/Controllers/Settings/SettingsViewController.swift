//
//  SettingsViewController.swift
//  Rocket.Chat
//
//  Created by Rafael Kellermann Streit on 14/02/17.
//  Copyright © 2017 Rocket.Chat. All rights reserved.
//

import UIKit
import SafariServices

final class SettingsViewController: UITableViewController {

    private let viewModel = SettingsViewModel()

    @IBOutlet weak var labelMobile: UILabel!    
    
    @IBAction func buttonCloseDidPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    var subscription: Subscription!
    let settingStrings = [
        "all": "全てのメッセージ",
        "mentions": "メンションのみ",
        "nothing": "なし"
        ]

    override func viewWillAppear(_ animated: Bool) {
        // 設定情報を取得
        labelMobile.text = ""
        SettingManager.getRoomSubscriptionSettingOfMe(rid: subscription.rid, completion:{(response) in
            let key = response.result["result"]["mobilePushNotifications"].stringValue
            self.labelMobile.text = self.settingStrings[key]
        })
    }

    func didTapSetting(settingString :String) {
        SettingManager.saveNotificationSettings(rid: subscription.rid, setting: settingString, completion: {_ in 
            // テーブルに反映
            self.labelMobile.text = self.settingStrings[settingString]
        })
    }
    
    // MARK: UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            tableView.deselectRow(at: indexPath, animated: true)
            
            // 通知設定
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            for key in self.settingStrings.keys {
                alert.addAction(UIAlertAction(title: self.settingStrings[key], style: .default, handler: { (_) in
                    self.didTapSetting(settingString: key)
                } ))
            }
            alert.addAction(UIAlertAction(title: "キャンセル", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        }
        if indexPath.section == 1 {
            // ライセンス表示
            guard let url = viewModel.settingsURL(atIndex:2) else { return }

            let controller = SFSafariViewController(url: url)
            navigationController?.pushViewController(controller, animated: true)
        }
        if indexPath.section == 2 {
            // ヘルプ表示
            let url = URL(string: "https://et-help.bizsky.jp/")
            
            let controller = SFSafariViewController(url: url!)
            navigationController?.pushViewController(controller, animated: true)
        }
        if indexPath.section == 3 {
            // ログアウト
            AuthManager.logout {
                // Cookie情報のクリア
                CookieManager.clearCookieString()
                
                // websocketのclose
                SocketManager.disconnect({ (_, _) in })

                let storyboardChat = UIStoryboard(name: "Main", bundle: Bundle.main)
                let controller = storyboardChat.instantiateInitialViewController()
                let application = UIApplication.shared
                
                if let window = application.keyWindow {
                    window.rootViewController = controller
                    window.makeKeyAndVisible()
                }
            }
            return
        }
        
        // 設定更新
//        guard let url = viewModel.settingsURL(atIndex: indexPath.row) else { return }
//        
//        let controller = SFSafariViewController(url: url)
//        navigationController?.pushViewController(controller, animated: true)

        
    }
}
