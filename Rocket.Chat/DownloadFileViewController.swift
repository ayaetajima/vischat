//
//  DownloadFileViewController.swift
//  Rocket.Chat
//
//  Created by kota on 2017/08/30.
//  Copyright © 2017年 Rocket.Chat. All rights reserved.
//

import UIKit

class DownloadFileViewController: UIViewController ,UIWebViewDelegate{

    @IBOutlet weak var web: UIWebView!
    
    @IBOutlet weak var navigationTitle: UINavigationItem!
    var urlStr : String = ""
    var titleStr : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        web.delegate = self

        let fileURL = URL(fileURLWithPath: urlStr)
        let req = NSURLRequest(url:fileURL )
        web.loadRequest(req as URLRequest)
        
        navigationTitle.title = titleStr
        
        web.scalesPageToFit = true
        web.contentMode = UIViewContentMode.scaleAspectFit
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadFileURL(url :String,title:String) {
        urlStr = url
        titleStr = title
    }
    
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        print(error)

        let alert = UIAlertController(
            title: "表示できないファイルです",
            message: "",
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(title: "閉じる", style: .default, handler: {_ in
            self.dismiss(animated: true, completion: nil);
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {

    }

    @IBAction func closeAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil);
    }
    
    override func viewDidAppear(_ animated: Bool) {

    }

}
