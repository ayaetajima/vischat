//
//  SubscriptionManager.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 7/9/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

struct SubscriptionManager {

    // swiftlint:disable function_body_length
    static func updateSubscriptions(_ auth: Auth, completion: @escaping MessageCompletion) {
        var params: [[String: Any]] = []

        if let lastUpdated = auth.lastSubscriptionFetch {
            params.append(["$date": Date.intervalFromDate(lastUpdated)])
        }

        let requestSubscriptions = [
            "msg": "method",
            "method": "subscriptions/get",
            "params": params
        ] as [String : Any]

        let requestRooms = [
            "msg": "method",
            "method": "rooms/get",
            "params": params
        ] as [String : Any]

        SocketManager.send(requestSubscriptions) { response in
            guard !response.isError() else { return Log.debug(response.result.string) }

            let subscriptions = List<Subscription>()

            // List is used the first time user opens the app
            let list = response.result["result"].array

            // Update & Removed is used on updates
            let updated = response.result["result"]["update"].array
            let removed = response.result["result"]["remove"].array

            list?.forEach { object in
                let subscription = Subscription.getOrCreate(values: object, updates: { (object) in
                    object?.auth = auth
                })

                subscriptions.append(subscription)
            }

            updated?.forEach { object in
                let subscription = Subscription.getOrCreate(values: object, updates: { (object) in
                    object?.auth = auth
                })

                subscriptions.append(subscription)
            }

            removed?.forEach { object in
                let subscription = Subscription.getOrCreate(values: object, updates: { (object) in
                    object?.auth = nil
                })

                subscriptions.append(subscription)
            }

            Realm.execute { realm in
                auth.lastSubscriptionFetch = Date()

                realm.add(subscriptions, update: true)
                realm.add(auth, update: true)
            }

            SocketManager.send(requestRooms) { response in
                guard !response.isError() else { return Log.debug(response.result.string) }

                let subscriptions = List<Subscription>()

                // List is used the first time user opens the app
                let list = response.result["result"].array

                // Update is used on updates
                let updated = response.result["result"]["update"].array

                Realm.execute { realm in
                    list?.forEach { object in
                        if let rid = object["_id"].string {
                            if let subscription = Subscription.find(rid: rid, realm: realm) {
                                subscription.roomDescription = object["description"].string ?? ""
                                subscription.roomTopic = object["topic"]["body"].string ?? ""
                                subscriptions.append(subscription)
                            }
                        }
                    }

                    updated?.forEach { object in
                        if let rid = object["_id"].string {
                            if let subscription = Subscription.find(rid: rid, realm: realm) {
                                subscription.roomDescription = object["description"].string ?? ""
                                subscription.roomTopic = object["topic"]["body"].string ?? ""
                                subscriptions.append(subscription)
                            }
                        }
                    }

                    auth.lastSubscriptionFetch = Date()
                    realm.add(subscriptions, update: true)
                }

                completion(response)
            }
        }
    }

    static func changes(_ auth: Auth) {
        let eventName = "\(auth.userId ?? "")/subscriptions-changed"
        let request = [
            "msg": "sub",
            "name": "stream-notify-user",
            "params": [eventName, false]
        ] as [String : Any]

        SocketManager.subscribe(request, eventName: eventName) { response in
            guard !response.isError() else { return Log.debug(response.result.string) }

            let msg = response.result["fields"]["args"][0]
            let object = response.result["fields"]["args"][1]

            let subscription = Subscription.getOrCreate(values: object, updates: { (object) in
                object?.auth = msg == "removed" ? nil : auth
            })

            Realm.execute { realm in
                realm.add(subscription, update: true)
            }
        }
    }

    // MARK: Search

    static func spotlight(_ text: String, rooms: Bool, users: Bool, completion: @escaping MessageCompletionObjectsList<Subscription>) {
        
        let request = [
            "msg": "method",
            "method": "spotlight",
            "params": [text, NSNull(), ["rooms": rooms, "users": users]]
        ] as [String : Any]
        
        SocketManager.send(request) { response in
            guard !response.isError() else {
                completion([])
                return Log.debug(response.result.string)
            }

            var subscriptions = [Subscription]()
            let rooms = response.result["result"]["rooms"].array
            let users = response.result["result"]["users"].array

            rooms?.forEach { object in
                let subscription = Subscription.getOrCreate(values: object, updates: { (object) in
                    object?.rid = object?.identifier ?? ""
                })

                subscriptions.append(subscription)
            }

            users?.forEach { object in
                let user = User.getOrCreate(values: object, updates: nil)
                let subscription = Subscription()
                subscription.identifier = user.identifier ?? ""
                subscription.otherUserId = user.identifier
                subscription.type = .directMessage
                subscription.name = user.username ?? ""
                subscriptions.append(subscription)
            }

            Realm.execute { realm in
                realm.add(subscriptions, update: true)
            }

            completion(subscriptions)
        }
    }

    static func messageSearch(room rid: String, searchText: String, subscription: Subscription, completion: @escaping MessageCompletionObjectsList<Message>) -> List<Message> {
        let request = [
            "msg": "method",
            "method": "messageSearch",
            "id" : "f4mManx2cKboRgazJ",
            "params":[searchText,rid]
            ]as [String : Any]
        
        let validMessages = List<Message>()

        SocketManager.send(request) { response in
            guard !response.isError() else { return Log.debug(response.result.string) }
            
            let messages = List<Message>()
            let list = response.result["result"]["messages"].array
            
            list?.forEach { object in
                let message = Message.getOrCreate(values: object, updates: { (object) in
                    object?.subscription = subscription
                })
                
                messages.append(message)
                
                if !message.userBlocked {
                    validMessages.append(message)
                }
            }
            
            Realm.update(messages)
            completion(Array(validMessages))
            
        }
//
//            SocketManager.send(request) { (response) in
//            guard !response.isError() else { return Log.debug(response.result.string) }
//            completion(response)
//        }
        
        return validMessages
    }
    
    // MARK: Rooms, Groups & DMs

    static func createNewChannel(_ channelName: String, isReadOnly: Bool, userName: String, completion: @escaping MessageCompletion) {
        let request = [
            "msg": "method",
            "method": "createChannel",
            "id": "85",
            "params": [
                channelName,
                [userName],
                isReadOnly
            ]
        ] as [String : Any]
        SocketManager.send(request) { response in
            guard !response.isError() else { return Log.debug(response.result.string) }
            completion(response)
        }
    }
    
    static func createPrivateGroup(_ channelName: String, isReadOnly: Bool, userName: String, completion: @escaping MessageCompletion) {
        let request = [
            "msg": "method",
            "method": "createPrivateGroup",
            "id": "89",
            "params": [
                channelName,
                [userName],
                isReadOnly
            ]
            ] as [String : Any]
        SocketManager.send(request) { response in
            guard !response.isError() else { return Log.debug(response.result.string) }
            completion(response)
        }
    }
    
    static func saveRoomSettings(_ rid: String, settings: [String], completion: @escaping MessageCompletion) {
        
        var params = [String]()
        params.append(rid)
        params.append(contentsOf: settings)
        let request = [
            "msg": "method",
            "method": "saveRoomSettings",
            "id": "16",
            "params": params
            ] as [String : Any]
        SocketManager.send(request) { response in
            guard !response.isError() else { return Log.debug(response.result.string) }
            
            let messages = response.result["messages"]
            let t = messages["t"]
            completion(response)
        }
    }
    
    static func createDirectMessage(_ username: String, completion: @escaping MessageCompletion) {
        let request = [
            "msg": "method",
            "method": "createDirectMessage",
            "params": [username]
        ] as [String : Any]

        SocketManager.send(request) { response in
            guard !response.isError() else { return Log.debug(response.result.string) }
            completion(response)
        }
    }

    static func getRoom(byName name: String, completion: @escaping MessageCompletion) {
        let request = [
            "msg": "method",
            "method": "getRoomByTypeAndName",
            "params": ["c", name]
        ] as [String : Any]

        SocketManager.send(request) { response in
            guard !response.isError() else { return Log.debug(response.result.string) }
            completion(response)
        }
    }

    static func join(room rid: String, completion: @escaping MessageCompletion) {
        let request = [
            "msg": "method",
            "method": "joinRoom",
            "params": [rid]
        ] as [String : Any]

        SocketManager.send(request) { (response) in
            guard !response.isError() else { return Log.debug(response.result.string) }
            completion(response)
        }
    }
    
    static func leaveRoom(room rid: String, completion: @escaping MessageCompletion) {
        let request = [
            "msg": "method",
            "method": "leaveRoom",
            "id" : "f4mManx2cKboRgazJ",
            "params": [rid]
            ] as [String : Any]
        
        SocketManager.send(request) { (response) in
            guard !response.isError() else { return Log.debug(response.result.string) }
            completion(response)
        }
    }

    static func getRoomInformation(room rid: String, completion: @escaping MessageCompletion) {
        let request = [
            "msg": "method",
            "method": "getRoomInformation",
            "id" : "f4mManx2cKboRgazJ",
            "params":["GENERAL"]
            ] as [String : Any]
        
        SocketManager.send(request) { (response) in
            guard !response.isError() else { return Log.debug(response.result.string) }
            
            completion(response)
        }
    }
    
    static func getUsersOfRoom(room rid: String, completion: @escaping MessageCompletionObjectsList<JSON>){

        let request = [
            "msg": "method",
            "method": "getUsersOfRoom",
            "id" : "f4mManx2cKboRgazJ",
            "params":[rid,true]
            ] as [String : Any]
        
        SocketManager.send(request) { (response) in
            guard !response.isError() else { return Log.debug(response.result.string) }
            let list = response.result["result"]["users"].arrayValue

            completion(list)
        }
    }

    // ファイル一覧
    static func getFileList(room rid: String, completion: @escaping MessageCompletionObjectsList<JSON>) {
        
        let request = [
            "msg": "method",
            "method": "getFilesOfRoom",
            "id" : "f4mManx2cKboRgazJ",
            "params": [ rid ]
            ] as [String : Any]

        SocketManager.send(request) { response in
            guard !response.isError() else { return Log.debug(response.result.string) }
        
            let list = response.result["result"].arrayValue
            completion(list)
        }
    }
    
    // ファイル削除
    static func deleteFile(fileName name: String, completion: @escaping VoidCompletion) {
        
        let request = [
            "msg": "method",
            "method": "deleteFileMessage",
            "id" : String.random(18),
            "params": [ name ]
            ] as [String : Any]
        
        SocketManager.send(request) { response in
            completion()
        }
    }
    
    static func getRoomSubscriptionSettingOfMe(room rid: String, completion: @escaping MessageCompletion) {
        let request = [
            "msg": "method",
            "method": "getRoomSubscriptionSettingOfMe",
            "id" : "f4mManx2cKboRgazJ",
            "params": [rid]
            ] as [String : Any]
        
        SocketManager.send(request) { (response) in
            guard !response.isError() else { return Log.debug(response.result.string) }
            completion(response)
        }
    }
    
    static func addUserToRoom(room rid: String, username: String, completion: @escaping MessageCompletion) {
        let request = [
            "msg": "method",
            "method": "addUserToRoom",
            "id" : "12354",
            "params" : [[
                    "rid" : rid,
                    "username" : username
                    ]]
            ] as [String : Any]
        
        SocketManager.send(request) { (response) in
            guard !response.isError() else { return Log.debug(response.result.string) }
            completion(response)
        }
    }
    
    // MARK: Messages

    static func markAsRead(_ subscription: Subscription, completion: @escaping MessageCompletion) {
        let request = [
            "msg": "method",
            "method": "readMessages",
            "params": [subscription.rid]
        ] as [String : Any]

        SocketManager.send(request) { response in
            guard !response.isError() else { return Log.debug(response.result.string) }
            completion(response)
        }
    }
    
    static func markReadOnRoom(room rid: String, completion: @escaping MessageCompletion){
        
        let request = [
            "msg": "method",
            "method": "markReadOnRoom",
            "id" : "f4mManx2cKboRgazJ",
            "params":[rid,true]
            ] as [String : Any]
        
        SocketManager.send(request) { (response) in
            guard !response.isError() else { return Log.debug(response.result.string) }
            completion(response)
        }
    }
    
    static func sendTextMessage(_ message: String, subscription: Subscription, completion: @escaping MessageCompletion) {
        let request = [
            "msg": "method",
            "method": "sendMessage",
            "params": [[
                "_id": String.random(18),
                "rid": subscription.rid,
                "msg": message,
            ]]
        ] as [String : Any]

        SocketManager.send(request) { (response) in
            guard !response.isError() else { return Log.debug(response.result.string) }
            completion(response)
        }
    }

    static func sendStampMessage(_ message: String, subscription: Subscription, completion: @escaping MessageCompletion) {
        let request = [
            "msg": "method",
            "method": "sendMessage",
            "params": [[
                "_id": String.random(18),
                "rid": subscription.rid,
                "msg": message,
                "t": "stamp"
                ]]
            ] as [String : Any]
        
        SocketManager.send(request) { (response) in
            guard !response.isError() else { return Log.debug(response.result.string) }
            completion(response)
        }
    }
    
    static func toggleFavorite(_ subscription: Subscription, completion: @escaping MessageCompletion) {
        let request = [
            "msg": "method",
            "method": "toggleFavorite",
            "params": [subscription.rid, !subscription.favorite]
        ] as [String : Any]

        SocketManager.send(request, completion: completion)
    }
}
