//
//  MessageManager.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 7/14/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

struct MessageManager {
    static let historySize = 30
}

let kBlockedUsersIndentifiers = "kBlockedUsersIndentifiers"

extension MessageManager {

    static var blockedUsersList = UserDefaults.standard.value(forKey: kBlockedUsersIndentifiers) as? [String] ?? []

    static func getHistory(_ subscription: Subscription, lastMessageDate: Date?, completion: @escaping MessageCompletionObjectsList<Message>) {
        var lastDate: Any!

        if let lastMessageDate = lastMessageDate {
            lastDate = ["$date": lastMessageDate.timeIntervalSince1970 * 1000]
        } else {
            lastDate = NSNull()
        }

        let request = [
            "msg": "method",
            "method": "loadHistory",
            "params": ["\(subscription.rid)", lastDate, historySize, [
                "$date": Date().timeIntervalSince1970 * 1000
            ]]
        ] as [String : Any]

        SocketManager.send(request) { response in
            guard !response.isError() else { return Log.debug(response.result.string) }

            let validMessages = List<Message>()
            let messages = List<Message>()
            let list = response.result["result"]["messages"].array

            list?.forEach { object in
                let message = Message.getOrCreate(values: object, updates: { (object) in
                    object?.subscription = subscription
                })
                
                messages.append(message)

                if !message.userBlocked {
                    validMessages.append(message)
                }
            }

            Realm.update(messages)
            completion(Array(validMessages))
        }
    }

    static func changes(_ subscription: Subscription) {
        let eventName = "\(subscription.rid)"
        let request = [
            "msg": "sub",
            "name": "stream-room-messages",
            "params": [eventName, false]
        ] as [String : Any]

        SocketManager.subscribe(request, eventName: eventName) { response in
            guard !response.isError() else { return Log.debug(response.result.string) }

            let object = response.result["fields"]["args"][0]
            let message = Message.getOrCreate(values: object, updates: { (object) in
                object?.subscription = subscription
            })

            Realm.update(message)
        }
    }

    static func report(_ message: Message, genre: String, completion: @escaping MessageCompletion) {
        guard let messageIdentifier = message.identifier else { return }

        let request = [
            "msg": "method",
            "method": "insertInappropriateTalk",
            "id" : "f4mManx2cKboRgazJ",
            "params": [messageIdentifier, genre]
        ] as [String : Any]

        SocketManager.send(request) { response in
            guard !response.isError() else { return Log.debug(response.result.string) }
            completion(response)
        }
    }

    static func delete(_ message: Message, completion: @escaping MessageCompletion) {
        guard let messageIdentifier = message.identifier else { return }
        
        let request = [
            "msg": "method",
            "method": "deleteMessage",
            "params": [[ "_id": messageIdentifier] ]
            ] as [String : Any]
        
        SocketManager.send(request) { response in
            guard !response.isError() else { return Log.debug(response.result.string) }
            
            Realm.execute({ realm in
                let results = realm.objects(Message.self).filter("identifier = '\(messageIdentifier)'")
                realm.delete(results)
            })
            
            completion(response)
        }
    }
    
    static func editMessage(_ message: Message, completion: @escaping MessageCompletion) {
        guard let messageIdentifier = message.identifier else { return }
        
        let request = [
            "msg": "method",
            "method": "updateMessage",
            "id": "42",
            "params": [[
                "_id": messageIdentifier,
                "rid": message.rid,
                "msg": message.text,
                ]]
            ] as [String : Any]
        
        SocketManager.send(request) { response in
            guard !response.isError() else { return Log.debug(response.result.string) }
            
            // realm update
            Realm.execute({ realm in
                let results = realm.objects(Message.self).filter("identifier = '\(messageIdentifier)'")
                
                Realm.update(results)
            })
            
            completion(response)
        }
    }

    static func pin(_ message: Message, completion: @escaping MessageCompletion) {
        guard let messageIdentifier = message.identifier else { return }

        let request = [
            "msg": "method",
            "method": "pinMessage",
            "params": [ ["rid": message.rid, "_id": messageIdentifier ] ]
        ] as [String : Any]

        SocketManager.send(request, completion: completion)
    }

    static func unpin(_ message: Message, completion: @escaping MessageCompletion) {
        guard let messageIdentifier = message.identifier else { return }

        let request = [
            "msg": "method",
            "method": "unpinMessage",
            "params": [ ["rid": message.rid, "_id": messageIdentifier ] ]
        ] as [String : Any]

        SocketManager.send(request, completion: completion)
    }
    
    static func star(_ message: Message, completion: @escaping MessageCompletion) {
        guard let messageIdentifier = message.identifier else { return }
        
        let request = [
            "msg": "method",
            "method": "starMessage",
            "id": "21",
            "params": [ ["_id": messageIdentifier,"rid": message.rid,"starred": true ] ]
            ] as [String : Any]
    
        
        SocketManager.send(request, completion: completion)
    }
    
    static func unstar(_ message: Message, completion: @escaping MessageCompletion) {
        guard let messageIdentifier = message.identifier else { return }
       
        let request = [
            "msg": "method",
            "method": "starMessage",
            "id": "21",
            "params": [ ["_id": messageIdentifier,"rid": message.rid,"starred": false ] ]
            ] as [String : Any]
        
        SocketManager.send(request, completion: completion)
    }

    static func blockMessagesFrom(_ user: User, completion: @escaping VoidCompletion) {
        guard let userIdentifier = user.identifier else { return }

        var blockedUsers: [String] = UserDefaults.standard.value(forKey: kBlockedUsersIndentifiers) as? [String] ?? []
        blockedUsers.append(userIdentifier)
        UserDefaults.standard.setValue(blockedUsers, forKey: kBlockedUsersIndentifiers)
        self.blockedUsersList = blockedUsers

        Realm.execute { (realm) in
            let messages = realm.objects(Message.self).filter("user.identifier = '\(userIdentifier)'")

            for message in messages {
                message.userBlocked = true
            }

            realm.add(messages, update: true)
        }

        completion()
    }
    
    static func getMessageList(_ request:[String : Any],subscription: Subscription, completion: @escaping MessageCompletionObjectsList<Message>) -> List<Message>
    {
        let messages = List<Message>()
        
        SocketManager.send(request) { response in
            guard !response.isError() else { return Log.debug(response.result.string) }
            
            let result = response.result
            let list = result["result"].arrayValue
            
            list.forEach { object in
                let message = Message.getOrCreate(values: object , updates: { (object) in
                    object?.subscription = subscription
                })
                
                messages.append(message)
            }
            
            Realm.update(messages)
            completion(Array(messages))
        }
        
        return messages
    }
    
    // ピン一覧
    static func getPinnedList(_ subscription: Subscription, completion: @escaping MessageCompletionObjectsList<Message>) -> List<Message>
    {
        let request = [
            "msg": "method",
            "method": "loadPinList",
            "id" : "f4mManx2cKboRgazJ",
            "params": [ subscription.rid ]
            ] as [String : Any]

        return self.getMessageList(request, subscription: subscription, completion: completion)
    }

    // メンション一覧
    static func getMentionList(_ subscription: Subscription, completion: @escaping MessageCompletionObjectsList<Message>) -> List<Message>
    {
        
        let request = [
            "msg": "method",
            "method": "loadMentionsToMe",
            "id" : "f4mManx2cKboRgazJ",
            "params": [ subscription.rid ]
            ] as [String : Any]
        
        return self.getMessageList(request, subscription: subscription, completion: completion)
    }
    
    // ブックマーク一覧
    static func getStaredList(_ subscription: Subscription, completion: @escaping MessageCompletionObjectsList<Message>) -> List<Message>
    {
        
        let request = [
            "msg": "method",
            "method": "loadStarList",
            "id" : "f4mManx2cKboRgazJ",
            "params": [ subscription.rid ]
            ] as [String : Any]

        return self.getMessageList(request, subscription: subscription, completion: completion)
    }
    
    // スタンプ一覧
    static func getListEmojiCustom(_ completion: @escaping MessageCompletionObjectsList<CustomStamp>)
    {
        let request = [
            "msg": "method",
            "method": "listEmojiCustom",
            "id" : "f4mManx2cKboRgazJ",
            "params": []
            ] as [String : Any]
        
        let stamps = List<CustomStamp>()
        
        SocketManager.send(request) { response in
            guard !response.isError() else { return Log.debug(response.result.string) }
            
            let result = response.result
            let list = result["result"].arrayValue
            
            list.forEach { object in
                let stamp = CustomStamp.getOrCreate(values: object , updates: nil)
                stamps.append(stamp)
            }
            
            // スタンプは毎回取得するので保存しないように
//            Realm.update(stamps)

            completion(Array(stamps))
        }
    }
}
