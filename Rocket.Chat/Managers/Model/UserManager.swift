//
//  UserManager.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 8/17/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift
 
struct UserManager {

    static func changes() {
        let request = [
            "msg": "sub",
            "name": "activeUsers",
            "params": []
        ] as [String : Any]

        SocketManager.send(request) { _ in }
    }

    static func userDataChanges() {
        let request = [
            "msg": "sub",
            "name": "userData",
            "params": []
        ] as [String : Any]

        SocketManager.send(request) { _ in }
    }

    static func setUserStatus(status: UserStatus, completion: @escaping MessageCompletion) {
        let request = [
            "msg": "method",
            "method": "UserPresence:setDefaultStatus",
            "params": [status.rawValue]
        ] as [String : Any]

        SocketManager.send(request) { (response) in
            completion(response)
        }
    }

    static func setUserPresence(status: UserPresence, completion: @escaping MessageCompletion) {
        let method = "UserPresence:".appending(status.rawValue)

        let request = [
            "msg": "method",
            "method": method,
            "params": []
        ] as [String : Any]

        SocketManager.send(request) { (response) in
            completion(response)
        }
    }
    
    // 一人分のUser情報取得
    static func getUserInfoOfName(username: String, completion: @escaping MessageCompletionObject<User>) {
        
        let request = [
            "msg": "method",
            "method": "getUserInfoOfName",
            "params": [ username ]
            ] as [String : Any]
        
        SocketManager.send(request) { (response) in
            guard !response.isError() else { return Log.debug(response.result.string) }
            
            var user: User!
            Realm.execute { realm in
                user = User.getOrCreate(values: response.result["result"], updates: nil)
            }
            completion(user)
        }
    }
    
    // 複数のUser情報一括取得
    static func getUserInformationByUsernames(usernames: [String], completion: @escaping MessageCompletionObjectsList<JSON>) {
        
        let request = [
            "msg": "method",
            "method": "getUserInformationByUsernames",
            "id" : "f4mManx2cKboRgazJ",
            "params": [ usernames ]
            ] as [String : Any]
        
        SocketManager.send(request) { (response) in
            guard !response.isError() else { return Log.debug(response.result.string) }
            let list = response.result["result"].arrayValue

            completion(list)
        }
    }

}
