//
//  SettingManager.swift
//  Rocket.Chat
//
//  Created by Rafael K. Streit on 8/17/16.
//  Copyright © 2016 Rocket.Chat. All rights reserved.
//

import Foundation
import SwiftyJSON

struct SettingManager {

    static func saveNotificationSettings(rid: String, setting: String, completion: @escaping MessageCompletion) {
        let request = [
            "msg": "method",
            "method": "saveNotificationSettings",
            "id" : "f4mManx2cKboRgazJ",
            "params": [ rid, "mobilePushNotifications", setting]
        ] as [String : Any]
        
        SocketManager.send(request) { (response) in
            completion(response)
        }
    }

    static func getRoomSubscriptionSettingOfMe(rid: String, completion: @escaping MessageCompletion) {
        
        let request = [
            "msg": "method",
            "method": "getRoomSubscriptionSettingOfMe",
            "id" : "f4mManx2cKboRgazJ",
            "params": [ rid ]
            ] as [String : Any]
        
        SocketManager.send(request) { (response) in
            guard !response.isError() else { return Log.debug(response.result.string) }
            completion(response)
        }
    }
}
