//
//  CookieManager.swift
//  Rocket.Chat
//
//  Created by Gradler Kim on 2017. 1. 23..
//  Copyright © 2017 Rocket.Chat. All rights reserved.
//

import Foundation

final class CookieManager {

    static let userDefaultKey_LoginUserId = "LoginUserId"
    static let userDefaultKey_LoginName = "LoginName"
    static let userDefaultKey_LoginNameSub = "LoginNameSub"
    static let userDefaultKey_LoginTenantId = "LoginTenantId"
    static let userDefaultKey_LoginTenantName = "LoginTenantName"
    
    static func getCookieString() -> String {
        // cookieを作成
        let ud = UserDefaults.standard
        let tenantID = ud.string(forKey: CookieManager.userDefaultKey_LoginTenantId)
        let userId = ud.string(forKey: CookieManager.userDefaultKey_LoginUserId)
        let name = ud.string(forKey: CookieManager.userDefaultKey_LoginName)
        let nameSub = ud.string(forKey: CookieManager.userDefaultKey_LoginNameSub)
        
        var cookie = ""
        if tenantID != nil && userId != nil && name != nil && nameSub != nil {
            cookie = "tenant_id=" + tenantID!
            cookie += ";user_id=" + userId!
            cookie += ";name=" + name!
            cookie += ";name_sub=" + nameSub! + ";"
        }

        return cookie
    }
    
    static func clearCookieString() {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: CookieManager.userDefaultKey_LoginUserId)
        userDefaults.removeObject(forKey: CookieManager.userDefaultKey_LoginName)
        userDefaults.removeObject(forKey: CookieManager.userDefaultKey_LoginNameSub)
        userDefaults.removeObject(forKey: CookieManager.userDefaultKey_LoginTenantId)
        userDefaults.synchronize()
        
        SocketManager.clearCookies()
    }

}
